import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { Router, ActivatedRoute } from  '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as _ from 'lodash'
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import {ElectronService} from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'inventory-ledger',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                MediaMatcher, 
                ElectronService,
                ItemDebitCreditService],
    templateUrl: 'inventoryledger.html',
    styleUrls: ['inventoryledger.css']
})

export class inventoryledger {
  
  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;

  allDebitCredits: any = []
  productsToUpdate: any = []
  productTotalStock: any = 0
  itemDetailFlag: boolean = false

  sub: any;
  productId: any = null
  selectedProduct: any = null

  selectedItem: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _electronService: ElectronService, 
              private _storeSettingsService: StoreSettingsService, 
              private _activatedRoute: ActivatedRoute, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _cdr: ChangeDetectorRef, 
              private _media: MediaMatcher, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }



  ngOnInit(){
    // var self = this;
    // self.getAllProducts()
    var self = this;
    self.sub = self._activatedRoute.params.subscribe(params => {
      self.productId = params['productId']; // (+) converts string 'id' to a number
      // console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.productId)
      self._productService.getSingleProduct(self.productId).then(function(result){
        console.log("SINGLE PRODUCT:====", result)
        self.selectedItem = result.docs[0]
        self.allDebitCredits =[]
        self._itemDebitCreditService.printDebitCredits(result.docs[0]).then(function(result){
          console.log("RESULT FROM SINGLE PRODUCT DEBTI CREDIT:====", result)
          result.docs.forEach(function(doc){
            self.allDebitCredits.push(doc)
          })
          // self.selectedProduct = result.docs[0]
          // self.allDebitCredits = _.orderBy(self.allDebitCredits, ['createdat'], ['asc']);
          console.log("ALL DEBIT CREDITS TO PRINT:------", self.allDebitCredits)
          self._electronService.ipcRenderer.send('PrintInventoryLedger')
        }).catch(function(err){
            console.log(err)
        })
      }).catch(function(err){
          console.log(err)

      })
    })
  }

  hideItemDetails(){
    this.itemDetailFlag = false
  }

  logout(){
    this._loginService.logout()
  }

  ngOnDestroy(): void {
    // this.mobileQuery.removeListener(this._mobileQueryListener);
  }  

}
