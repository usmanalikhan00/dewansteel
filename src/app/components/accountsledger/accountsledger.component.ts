import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
import { Router, ActivatedRoute } from  '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as _ from 'lodash'
import * as moment from 'moment'
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import { ElectronService } from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'accounts-ledger',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                CustomersService, 
                MediaMatcher, 
                ElectronService, 
                AccountTypesService, 
                AccountDebitCreditService, 
                ItemDebitCreditService],
    templateUrl: 'accountsledger.html',
    styleUrls: ['accountsledger.css']
})

export class accountsledger {
  

  allDebitCredits: any = []
  allRefAccounts: any = []
  accountId: any = null
  sub: any = null
  selectedAccount: any = null


  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _storeSettingsService: StoreSettingsService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customersService: CustomersService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _accountTypesService: AccountTypesService, 
              private _electronService: ElectronService, 
              private _activatedRoute: ActivatedRoute, 
              private _cdr: ChangeDetectorRef, 
              private _media: MediaMatcher, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {

  }



  ngOnInit(){
    var self = this;
    self.sub = self._activatedRoute.params.subscribe(params => {
      self.accountId = params['accountId']; // (+) converts string 'id' to a number
      // console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.productId)
      self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
        console.log("SINGLE ACCOUNT:====", result)
        self.selectedAccount = result.docs[0]
        var sum = 0
        if (self.selectedAccount.accounttype === 'bank' || self.selectedAccount.accounttype === 'cash'){

          self.allDebitCredits = []
          self._accountDebitCreditService.getDebitCredits(result.docs[0]).then(function(result){
            console.log("RESULT FROM SINGLE ACCOUNT DEBTI CREDIT:====", result)
            result.docs.forEach(function(row){
              if (row.dctype === 'payment'){
                if (row.paymentdate){
                  row.dcrefdate = row.paymentdate
                }else{
                  row.dcrefdate = row._id
                }
              }
              else if (row.dctype === 'receipt'){
                if (row.receiptdate){
                  row.dcrefdate = row.receiptdate
                }else{
                  row.dcrefdate = row._id
                }
              }
              else if (row.dctype === 'transfer'){
                if (row.transferdate){
                  row.dcrefdate = row.transferdate
                }else{
                  row.dcrefdate = row._id
                }
              }
              self.allDebitCredits.push(row)
            })
            // self.selectedProduct = result.docs[0]
            // self.allDebitCredits = _.orderBy(self.allDebitCredits, ['createdat'], ['asc']);
            self.allDebitCredits = _.sortBy(self.allDebitCredits, function(o) { return moment(o.dcrefdate).format('YYYYMMDD'); });
            console.log("ALL DEBIT CREDITS TO PRINT:------", self.allDebitCredits)
            // self._electronService.ipcRenderer.send('Print')

            for (let i = 0; i<self.allDebitCredits.length; i++){
              if (i == 0){
                if (self.allDebitCredits[i].credit == null){
                  sum = self.selectedAccount.openingbalance + self.allDebitCredits[i].debit
                } 
                if (self.allDebitCredits[i].debit == null){
                  sum = self.selectedAccount.openingbalance + self.allDebitCredits[i].credit
                } 
                self.selectedAccount.closingbalance = sum
              }else{
                if (self.allDebitCredits[i].credit == null){
                  sum = self.selectedAccount.closingbalance + self.allDebitCredits[i].debit
                } 
                if (self.allDebitCredits[i].debit == null){
                  sum = self.selectedAccount.closingbalance + self.allDebitCredits[i].credit
                } 
                self.selectedAccount.closingbalance = sum
              }
              self.allDebitCredits[i].closingbalance = sum
              if (self.allDebitCredits[i].dctype === 'receipt' || self.allDebitCredits[i].dctype === 'payment'){
                if (self.allDebitCredits[i].notes)
                  self.allDebitCredits[i].notes = self.allDebitCredits[i].notes
                else
                  self.allDebitCredits[i].notes = null
              }else if (self.allDebitCredits[i].dctype === 'transfer'){
                if (self.allDebitCredits[i].transfernotes)
                  self.allDebitCredits[i].notes = self.allDebitCredits[i].transfernotes
                else
                  self.allDebitCredits[i].notes = null
              }
              if (self.allDebitCredits[i].closingbalance < 0){
                self.allDebitCredits[i].status = 'credit'
                self.selectedAccount.status = 'credit'
              }
              else{
                self.allDebitCredits[i].status = 'debit'
                self.selectedAccount.status = 'debit'
              }
            }




            self._electronService.ipcRenderer.send('PrintAccountLedger')
          }).catch(function(err){
              console.log(err)
          })
        }else{
          self.allRefAccounts = []
          self._accountTypesService.getChildLevelAccounts(result.docs[0]).then(function(result){
            console.log("RESULT FROM SINGLE ACCOUNT REFRENCES:====", result)
            result.docs.forEach(function(doc){
              self.allRefAccounts.push(doc)
            })
            // self.selectedProduct = result.docs[0]
            // self.allDebitCredits = _.orderBy(self.allDebitCredits, ['createdat'], ['asc']);
            console.log("ALL SINGLE ACCOUNTS TO PRINT:------", self.allRefAccounts)
            self._electronService.ipcRenderer.send('PrintAccountLedger')
            // self._electronService.ipcRenderer.send('Print')
          }).catch(function(err){
              console.log(err)
          })
        }
      }).catch(function(err){
          console.log(err)

      })
    })
  }


  logout(){
    this._loginService.logout()
  }

  ngOnDestroy(): void {
  }  

}
