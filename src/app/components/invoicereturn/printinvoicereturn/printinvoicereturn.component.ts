import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceReturnService } from '../../../services/invoicereturn.service'
import { PurchaseService } from '../../../services/purchase.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";
import {ElectronService} from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
    selector: 'print-invoicereturn',
    providers: [LoginService, ProductService, InvoiceReturnService, ElectronService, PurchaseService],
    templateUrl: 'printinvoicereturn.html',
    styleUrls: ['printinvoicereturn.css']
})

export class printinvoicereturn {
  

  sub: any;
  invoicereturnId: any;
  selectedInvoiceReturn: any;
  invoiceNotes: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceReturnService: InvoiceReturnService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _purchaseService: PurchaseService, 
              private _electronService: ElectronService, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.invoicereturnId = params['invoicereturnId']; // (+) converts string 'id' to a number
      console.log("INVOCIE RETURN ID:-----", self.invoicereturnId)
      self._invoiceReturnService.getSingleInvoiceReturn(self.invoicereturnId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
        self.selectedInvoiceReturn = result.docs[0]
        console.log("RESULT FROM SINLE INVOICE RETURN", self.selectedInvoiceReturn)
        self._electronService.ipcRenderer.send('PrintInvoiceReturn')
      }).catch(function(err){
          console.log(err)
      })
    });
  }
  
  // getSingleInvoice(){
  //   var self = this;
  //   self._invoiceService.getSingleInvoice(self.purchaseId).then(function(result){
  //     // console.log("RESULT FROM SINLE CATEGORY", result)
  //     self.selectedPurchase = result.docs[0]
  //     console.log("RESULT FROM SINLE CATEGORY", self.selectedPurchase)
  //   }).catch(function(err){
  //       console.log(err)
  //   })
  // }

  // goBack(){
  //   this._router.navigate(['invoicereturn'])
  // }

  // @HostListener('window:resize', ['$event'])
  //   onResize(event) {
  //       if (event.target.innerWidth < 886) {
  //           this.navMode = 'over';
  //           this.sidenav.close();
  //       }
  //       if (event.target.innerWidth > 886) {
  //          this.navMode = 'side';
  //          this.sidenav.open();
  //       }
  //   }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
