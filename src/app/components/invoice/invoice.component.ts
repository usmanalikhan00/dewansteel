import { Component, ElementRef, ViewChild, HostListener, Renderer2, Inject } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import {ElectronService} from 'ngx-electron'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { CustomersService } from '../../services/customers.service'
import * as _ from 'lodash' 


export interface UserData {
  invoicenumber ? : string;
  createdby ? : string;
  invoicecustomer ? : string;
  productcount ? : number;
  invoicenotes ? : string;
  invoicedate ? : string;
  invoicetotal ? : number;
  billno ? : number;
  challanno ? : number;
  othercharges ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
  found ? : boolean;
}

@Component({
    selector: 'invoice',
    providers: [LoginService, ProductService, InvoiceService, ElectronService, CustomerDebitCreditService, ItemDebitCreditService, CustomersService],
    templateUrl: 'invoice.html',
    styleUrls: ['invoice.css']
})

export class invoice {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  public invoicesDB = new PouchDB('steelcustomerinvoice');
  public localInvoicesDB = new PouchDB('http://localhost:5984/localsteelinvoices');
  // public ibmInvoicesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewaninvoices', {
  //   // auth: {
  //   //   "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
  //   //   "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
  //   //   "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
  //   //   "port": 443,
  //   //   "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
  //   // }
  //   auth: {
  //     'username':'usmanalikhan00@gmail.com',
  //     'password':'usmanali@eyetea.co'
  //   },
  //   ajax: {
  //               headers: {
  //                     Authorization: 'Basic ' + window.btoa('usmanalikhan00@gmail.com' + ':' + 'usmanali@eyetea.co')
  //               }
  //          }
  // });
  public ibmInvoicesDB = new PouchDB('https://dewansteel.cloudant.com/dewaninvoices', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });

  public cloudantInvoicesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewaninvoices', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  
  allInvoices: any = [];
  allInvoicesDC: any = [];
  allCustomers: any = [];
  invoicesTotal: any = null;
  
  dateFilterForm: FormGroup;

  displayedColumns = [
    "createdat",
    'invoicenumber',
    "challanno", 
    "billno", 
    'createdby', 
    'invoicecustomer', 
    'productcount', 
    "invoicenotes",
    "invoicetotal",
    "action"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null

  authUser: any = null
  animal: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customersService: CustomersService, 
              private _formBuilder: FormBuilder, 
              private _rendrer: Renderer2, 
              public _dialog: MatDialog,
              private _electronService: ElectronService, 
              private _router: Router) {
    this._buildAddProductForm();
    this.loadDataSource()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddProductForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }

  ngOnInit(){
    var self = this
    // self.getCustomerInvoiceDC()
    self.getAllCustomers()
    // self.syncDb()
    self.checkscreen()
  }

  checkscreen(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
  }



  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.invoicesDB.replicate.from(self.cloudantInvoicesDB).on('complete', function(info) {
      // console.log("COMPLETE EVENT FROM ONE-WAY INVOICES REPLICATION:--", info)
      self.invoicesDB.sync(self.cloudantInvoicesDB, opts).on('change', function (info) {
        // console.log("CHANGE EVENT FROM TWO-WAY INVOICES SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
          // self.loadDataSource()
        // }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC INVOICES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY INVOICES SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }

  getCustomerInvoiceDC(){
    var self = this
    var invoiceDC = 0
    self._customerDebitCreditService.getInvoiceDebitCredits().then(function(result){
      self.allInvoicesDC = result.docs
      for (let obj of self.allInvoicesDC){
        var dateKey = _.filter(self.allInvoices, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].invoicedate
        if (dateKey[0])
          invoiceDC++
      }
      // console.log("ALL DEBIT/CREDIT FOR INVOICE:-----", self.allInvoicesDC)
      // console.log("UPDATED INVOICE COUNT:-----", invoiceDC)
      // self.updateInvoiceDC(self.allInvoicesDC)
    }).catch(function(err){
      console.log(err)
    })
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }



  loadDataSource(){
    var self = this;
    var invoiceDC = 0
    var noInvoiceDC = 0
    self._invoiceService.allInvocies().then(function(result){
      const users: UserData[] = [];
      self.allInvoices = [];
      result.rows.map(function(row){
        // users.push(createNewUser(row.doc))
        self.allInvoices.push(row.doc)
      })
      // console.log("ALL INVOICES:=====", self.allInvoices);
      self._customerDebitCreditService.getInvoiceDebitCredits().then(function(result){
        self.allInvoicesDC = result.docs
        for (let obj of self.allInvoices){
          var dataKey = _.filter(self.allInvoicesDC, {dcrefid:obj._id})
          if (dataKey[0]){
            invoiceDC++
            obj.found = true
          }
          else{
            noInvoiceDC++
            obj.found = false
          }
          users.push(createNewUser(obj))
        }
        // console.log("ALL DEBIT/CREDIT FOR INVOICE:-----", self.allInvoicesDC)
        // console.log("UPDATED INVOICE COUNT:-----", invoiceDC, noInvoiceDC)
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getInvoicesTotal(self.allInvoices)
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err);
    })

  }

  goToPrint(){
    // this._router.navigate(['printinvoices'])
    this._electronService.ipcRenderer.send('InvoicesPrint')
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
      var self = this
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      self.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      self.toDate = event.value.toISOString()
    }
    // console.log("FROM DATE:----", "\n", self.fromDate, "TO DATE:----", self.toDate)

    if (self.fromDate < self.toDate){
      // console.log("FROM DATE  SMALL")
    }else{
      // console.log("FROM DATE LARGER")
    }
    if (self.toDate < self.fromDate){
      // console.log("TO DATE  SMALL")
    }else{
      // console.log("TO DATE LARGER")
    }

    if (self.toDate != null && self.fromDate != null){

      self._invoiceService.filterInvoices(self.toDate, self.fromDate).then(function(result){
        // console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        // self.allInvoices = []
        // result.docs.forEach(function(doc){
        //   self.allInvoices.push(doc)
        // })
        const users: UserData[] = [];
        self.allInvoices = [];
        result.docs.forEach(function(row){
          users.push(createNewUser(row))
          self.allInvoices.push(row)
        })
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getInvoicesTotal(self.allInvoices)
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.loadDataSource()
    this.toDate = null
    this.fromDate = null
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getInvoicesTotal(allInvoices){
    var sum = 0
    for (let doc of allInvoices){
      sum = sum + doc.invoicetotal
    }
    this.invoicesTotal = sum
    // this.invoicesTotal = Number(this.invoicesTotal)
    // console.log("SUM FROM INVOICES TOTAL:===", sum, this.invoicesTotal)
  }

  getSingleInvoice(invoice){
    // console.log("Selected Invoice:===", invoice)
    this._router.navigate(['/invoice', invoice._id])
  }

  openLedgerDialog(item): void {
    var self = this
    var selectedInvoice = null
    // console.log("ITEM CLICKED:------", item)
    self._invoiceService.getSingleInvoice(item._id).then(function(result){
      selectedInvoice = result.docs[0]
      if (!item.found){
        // console.log("RESULT FROM SINGLE INVOICE:--", result);
        // console.log("ITEM NOT FOUND IN DC");
        var customerToUpdate = _.filter(self.allCustomers, {"_id": result.docs[0].invoicecustomer._id})
        customerToUpdate[0].closingbalance = customerToUpdate[0].closingbalance + result.docs[0].invoicetotal
        var customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": result.docs[0].invoicecustomer.name,
          "address": result.docs[0].invoicecustomer.address,
          "phone": result.docs[0].invoicecustomer.phone,
          "fax": result.docs[0].invoicecustomer.fax,
          "email": result.docs[0].invoicecustomer.email,
          "customerid": customerToUpdate[0]._id,
          "customerrev": customerToUpdate[0]._rev,
          "openingbalance": customerToUpdate[0].openingbalance,
          "closingbalance": customerToUpdate[0].closingbalance,
          "dcref": result.docs[0].invoicenumber,
          "dcrefid": result.docs[0]._id,
          "dcrefdate": result.docs[0].invoicedate,
          "billno": result.docs[0].billno,
          "challanno": result.docs[0].challanno,
          "dctype": "invoice",
          "status": null,
          "debit": null,
          "credit": null
        }
        if (customerToUpdate[0].closingbalance >= 0){
          customerToUpdate[0].status = 'debit'
          customerDebitCredit.status = 'debit'
          customerDebitCredit.debit = result.docs[0].invoicetotal
          customerDebitCredit.closingbalance = customerToUpdate[0].closingbalance
        }else if (customerToUpdate[0].closingbalance <= 0){
          customerToUpdate[0].status = 'credit'
          customerDebitCredit.status = 'credit'
          customerDebitCredit.debit = result.docs[0].invoicetotal
          customerDebitCredit.closingbalance = customerToUpdate[0].closingbalance
        }
        // console.log("CUSTOMER DEBIT CREDIT OJECT:--", customerDebitCredit, customerToUpdate);
        self._customerDebitCreditService.addDebitCredit(customerDebitCredit).then(function(result){
          // console.log("RESULTA AFTER ADDING DEBIT CREDIT:--", result)
          self._customersService.updateCustomerBalance(customerToUpdate[0]).then(function(result){
            // console.log("RESULT UPDATING ACTUAL CUSTOMER:--", result)
            let dialogRef = self._dialog.open(invoiceDetailDialog, {
              width: '100%',
              height: '600px',
              data: { info: selectedInvoice, dcref: 'invoice' }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('The Ledger detail was closed', result);
              self.getAllCustomers()
              self.loadDataSource()
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }else{
        // console.log("RESULT FROM SINGLE INVOICE:--", result);
        // console.log("ITEM FOUNF IN DC:--");
        let dialogRef = self._dialog.open(invoiceDetailDialog, {
          width: '100%',
          height: '600px',
          data: { info: selectedInvoice, dcref: 'invoice' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('The Ledger detail was closed', result);
          // self.getAllCustomers()
        })
      }
    }).catch(function(err){
      console.log(err);
    })


  }


  openDeleteDialog(invoice): void {
    // var self = this;
    // console.log("PRODUCT CLICKED:------", product)
    let dialogRef = this._dialog.open(invoiceDeleteDialog, {
      width: '250px',
      data: { name: invoice.invoicenumber }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
      console.log('The dialog was closed', result, this.animal);
      if (this.animal){
        // alert(1)
        this.getInvoiceDc(invoice)
        // this.getProductPurchaseDc(purchase)
        // console.log('YES TYPED:--', purchase);
        this.removeInvoice(invoice)
      }
    })
  }

  removeInvoice(invoice){
    var self = this
    invoice._deleted = true
    self._invoiceService.addInvoice(invoice).then(function(result){
      console.log("RESULT AFTER DELETING PURCHASE:--", result)
      self.loadDataSource()
    }).catch(function(err){
      console.log(err)
    })
  }

  getInvoiceDc(invoice){
    var self = this;
    var customerDC: any = null
    var productsDC: any = []
    self._customerDebitCreditService.getSingleInvoiceDebitCredit(invoice).then(function(result){
      result.docs.forEach(function(row){
        row._deleted = true
        customerDC = row
      })
      console.log("RESULT FROM SINGLE ENTRY OF CUSTOMER DC:--", customerDC)
      self._itemDebitCreditService.getSinglePurchaseDebitCredit(invoice).then(function(result){
        result.docs.forEach(function(row){
          row._deleted = true
          productsDC.push(row)
        })
        console.log("RESULT FROM SINGLE ENTRY OF PRODUCT DC:--", productsDC)
        self._itemDebitCreditService.addDebitCredit(productsDC).then(function(result){
          console.log("RESULT AFTER DELETING PRODUCT DC:--", result)
          self._customerDebitCreditService.addDebitCredit(customerDC).then(function(result){
            console.log("RESULT AFTER DELETING CUSTOMER DC:--", result)
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })

  }  


  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {

  // console.log("ROW TO SHOW FOR PAYMENTS", row)

  return {
    "_id": row._id,
    "_rev": row._rev,
    "invoicecustomer" : row.invoicecustomer.name.toString(),
    "createdat" : row.createdat.toString(),
    "createdby" : row.createdby._id.toString(),
    "invoicenotes" : row.invoicenotes,
    "invoicenumber" : row.invoicenumber.toString(),
    "invoicetotal" : row.invoicetotal,
    "billno" : row.billno,
    "challanno" : row.challanno,
    "invoicedate" : row.invoicedate,
    "found": row.found,
    "productcount" : row.invoiceproducts.length
  };
}

@Component({
  selector: 'invoice-detail',
  templateUrl: 'invoice-detail.html',
  providers:[ElectronService],
  styleUrls: ['invoice.css']
})
export class invoiceDetailDialog {

  constructor(
    public dialogRef: MatDialogRef<invoiceDetailDialog>,
    public _electronService: ElectronService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(){
    // console.log("POP UP START DATA:---", this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(data){
    // console.log(data)
    this._electronService.ipcRenderer.send('loadPrintPage', data._id);
  }

}

@Component({
  selector: 'invoice-delete-dialog',
  templateUrl: 'invoice-delete-dialog.html'
})
export class invoiceDeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<invoiceDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}