import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import {InvoiceService} from '../../services/invoice.service'
import {PurchaseService} from '../../services/purchase.service'
import {ProductService} from '../../services/product.service'
// import { User } from '../../models/model-index'
import { AccountTypesService } from '../../services/accounttypes.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'deo-dashboard',
    providers: [LoginService],
    templateUrl: 'deodashboard.html',
    styleUrls: ['deodashboard.css']
})

export class deodashboard {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  users: any = [];
  addProductForm: FormGroup;

  email: string = null;
  password: string = null;

  authUser: any = null

  tiles = [
    {text: 'One', cols: 4, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 1, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 6, rows: 1, color: '#DDBDF1'},
  ];

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){

  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.authUser = JSON.parse(localStorage.getItem('user'))
    console.log("AUTH USER:-------", this.authUser)
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }
}