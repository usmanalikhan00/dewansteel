import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  purchasenumber ? : string;
  createdby ? : string;
  purchasecustomer ? : string;
  productcount ? : number;
  purchasenotes ? : string;
  purchasetotal ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'print-products',
    providers: [LoginService, ProductService, InvoiceService, PurchaseService, ElectronService],
    templateUrl: 'printproducts.html',
    styleUrls: ['printproducts.css']
})

export class printproducts {
  
  allProducts: any = [];
  productTotalStock: any = null;
  productsTotalWeight: any = null;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loadDataSource()
  }

  loadDataSource(){
    var self = this;
    // self.allInvoices = [];
    self._productService.allProducts().then(function(result){
      self.allProducts = [];
      result.rows.map(function(row){
        self.allProducts.push(row.doc); 
      })
      console.log("PRODUCTS", self.allProducts)
      self.calculateStockTotal(self.allProducts)
      // self.calculateStockWeight(self.allProducts)
      self._electronService.ipcRenderer.send('PrintProducts')
    }).catch(function(err){
      console.log(err);
    })

  }

  calculateStockTotal(products){
    var sum = 0
    var sum2 = 0
    for (let item of products){
      sum = sum + item.networth
      if (item.weight != "none"){
        sum2 = sum2 + item.netweight
      }
    }
    this.productTotalStock = sum
    this.productsTotalWeight = sum2
      console.log("NWT WEIGHT AFTER CALC:--", this.productsTotalWeight);
  }

  // calculateStockWeight(products){
  //   var sum = 0
  //   for (let item of products){
  //     sum = sum + item.netweight
  //   }
  // }



  // getSinglePurchase(purchase){
  //   console.log("Selected Purchase:===", purchase)
  //   this._router.navigate(['/purchase', purchase._id])
  // }

  logout(){
    this._loginService.logout()
  }  
}
