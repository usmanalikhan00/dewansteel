import { Component, ElementRef, ViewChild, HostListener, Output, Inject } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'

export interface UserData {
  name ? : string;
  address ? : string;
  phone ? : string;
  openingbalance ? : number;
  closingbalance ? : number;
  status ? : string;
  fax ? : string;
  email ? : string;
  obtype ? : string;
  _id ? : string;
  _rev ? : string;

}

export interface UserData2 {
  refdate ? : string;
  dctype ? : string;
  refnumber ? : string;
  refvoucherno ? : string;
  refchallanno ? : string;
  refbillno ? : string;
  receiptdate ? : string;
  paymentdate ? : string;
  refacc ? : string;
  notes ? : string;
  debit ? : number;
  credit ? : number;
  closing ? : number;
  status ? : string;
  dcrefid ? : string;
  _id ? : string;
  _rev ? : string;

}

@Component({
    selector: 'customers',
    providers: [LoginService, 
                ProductService, 
                CustomersService,
                InvoiceService,
                PurchaseService,
                CustomerDebitCreditService, 
                ElectronService],
    templateUrl: 'customers.html',
    styleUrls: ['customers.css']
})

export class customers {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  public customersDB = new PouchDB('steelcustomers');
  public localCustomersDB = new PouchDB('http://localhost:5984/localsteelcustomers');
  public ibmCustomersDB = new PouchDB('https://dewansteel.cloudant.com/dewancustomers', {
    // auth: {
    //   "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
    //   "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
    //   "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
    //   "port": 443,
    //   "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    // }
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
  public cloudantCustomersDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewancustomers', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });


  public customerDebitCreditDB = new PouchDB('steelcustomerdebitcredit');
  public localCustomerDebitCreditDB = new PouchDB('http://localhost:5984/localcustomerdebitcredit');
  public ibmCustomerDebitCreditDB = new PouchDB('https://dewansteel.cloudant.com/dewancustomerdebitcredit', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });

  public cloudantCustomerDebitCreditDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewancustomerdebitcredit', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });


  customers: any = [];
  addCustomerForm: FormGroup;
  dateFilterForm: FormGroup;
  editCustomerForm: FormGroup;
  addCustoemrFlag: boolean = false;
  allCustomers: any= [];
  balanceTypes = ['Credit', 'Debit']
  customerDebitCredits: any = []
  selectedCustomer: any = null
  customerDetailFlag: boolean = false
  editCustomerFlag: boolean = false

  displayedColumns = [
    'datetime',
    'name', 
    'address', 
    'phone', 
    'opening', 
    "closing",
    "status",
    'action'
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns2 = [
    'date',
    'type', 
    'refnumber', 
    'voucherno', 
    'billno', 
    "refacc",
    "notes",
    'debit',
    'credit',
    'closing',
    'status',
    'action',
  ];

  dataSource2: MatTableDataSource<UserData2>;

  @ViewChild(MatPaginator) paginator2: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;




  events: string[] = [];
  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null

  totalDebits: any = null
  totalCredits: any = null
  authUser: any = null
  animal: any = null
  editCustomerDetails: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder,
              public _dialog: MatDialog,  
              private _router: Router) {
    this._buildAddCustomerForm();
    this._buildEditCustomerForm();
    this._buildDateFilterForm();
    this.loadDataSource()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }

  private _buildEditCustomerForm(){
    this.editCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: [''],
      openingbalance:['', Validators.required],
      obtype:['', Validators.required]
    })
  }



  private _buildDateFilterForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }



  ngOnInit(){
    var self = this
    // self.syncDb()
    // self.syncDebitCredit()
    // self.getAllCustomers()
    // this.setDates()
    // var object = {
    //     "_id": new Date().toISOString(),
    //     "address":"ishfaq chowk jorye pull lahore",
    //     "billno":"272-A",
    //     "challanno":"239-A",
    //     "closingbalance":1277366.50,
    //     "credit":null,
    //     "customerid":"2018-03-05T12:17:35.341Z",
    //     "customerrev":"36-17b5122babbe4977b2a6c62b815e8bac",
    //     "dcref":100945,
    //     "dcrefid":"2018-07-18T09:51:11.258Z",
    //     "dctype":"invoice",
    //     "debit":391202.5,
    //     "email":null,
    //     "fax":null,
    //     "name":"A-S Traders",
    //     "openingbalance":611524,
    //     "phone":"0302-4496007",
    //     "status":"debit",
    //   }
    //   self._customerDebitCreditService.addDebitCredit(object).then(function(result){
    //     console.log("RESULT AFTER ADDING DEBIT CREDIT:=---", result)
    //   }).catch(function(err){
    //     console.log(err)
    //   })
  }




  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.customersDB, self.ibmCustomersDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull' || info.direction === 'push'){
    //     self.loadDataSource()
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM CUSTOMER", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.customersDB.replicate.from(self.cloudantCustomersDB).on('complete', function(info) {
      // console.log("COMPLETE EVENT FROM ONE-WAY CUSTOMER REPLICATION:--", info)
      self.customersDB.sync(self.cloudantCustomersDB, opts).on('change', function (info) {
        // console.log("CHANGE EVENT FROM TWO-WAY CUSTOMER SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
        //   // console.log("----PULL FROM TWO-WAY CUSTOMER SYNC-----")
        //   self.loadDataSource()
        // }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT TWO-WAY SYNC CUSTOMER", err)
      }).on('active', function () {
        console.log("****ACTIVE ACTIVE TWO-WAY SYNC CUSTOMER****")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    });
  }

  syncDebitCredit(){
    var self = this
    // var sync = PouchDB.sync(self.customerDebitCreditDB, self.ibmCustomerDebitCreditDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull'){
    //     self.customerDebitCredit(self.selectedCustomer)
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM CUSTOMER DEBIT CREDIT", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.customerDebitCreditDB.replicate.from(self.cloudantCustomerDebitCreditDB).on('complete', function(info) {
      // console.log("COMPLETE EVENT FROM ONE-WAY CUSTOMER DEBIT CREDIT REPLICATION:--", info)
      self.customerDebitCreditDB.sync(self.cloudantCustomerDebitCreditDB, opts).on('change', function (info) {
        // console.log("CHANGE EVENT FROM TWO-WAY CUSTOMER DEBIT CREDIT SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
        //   console.log("----PULL FROM TWO-WAY CUSTOMER DEBIT CREDIT SYNC-----")
        //   self.customerDebitCredit(self.selectedCustomer)
        // }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT TWO-WAY SYNC CUSTOMER DEBIT CREDIT", err)
      }).on('active', function () {
        console.log("****ACTIVE ACTIVE TWO-WAY SYNC CUSTOMER DEBIT CREDIT****")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    });
  }

  loadDataSource(){
    var self = this;
    self._customersService.allCustomers().then(function(result){
      // console.log("ALL RESULTS FROM CUSTOMER:=====", result);
      const users: UserData[] = [];
      result.rows.map(function(row){
        // row.doc.openingbalance = 0 
        // row.doc.closingbalance = 0 
        users.push(createNewUser(row.doc))
      })
      self.dataSource = new MatTableDataSource(users);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
    }).catch(function(err){
      console.log(err);
    })

  }

  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.customerDebitCredit(this.selectedCustomer)
  }

  setDates(){
    // var moment
    // console.log("DATE CHANGES:----", $event.target.value)
    // var inputyear = parseInt(moment($event.target.value,"YYYY/MM/DD").format("YYYY"))
    // var inputmonth = parseInt(moment($event.target.value,"YYYY/MM/DD").format("MM"))
    // var inputday = parseInt(moment($event.target.value,"YYYY/MM/DD").format("DD"))
    // console.log("YEAR AFTER FORMATING INPUT DATE:--\n", "INPUT YEAR\n", inputyear, typeof(inputyear), "INPUT MONTH\n", inputmonth, typeof(inputmonth), "INPUT DAYY\n", inputday, typeof(inputday))
    // var currentyear = parseInt(moment().format("YYYY"))
    // var currentmonth = parseInt(moment().format("MM"))
    // var currentday = parseInt(moment().format("DD"))
    // console.log("STATS OF TODAY:--", currentyear, typeof(currentyear), currentmonth, typeof(currentmonth), currentday, typeof(currentday))
    // this.minDate = new Date(currentyear, currentmonth-1, currentday);

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  applyFilter2(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource2.filter = filterValue;
  }




  refreshData($event){
    // console.log('fuck you!!', $event)
    this.loadDataSource()
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      this.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      this.toDate = event.value.toISOString()
    }
    console.log("TO DATE:----\n", this.toDate, "\n", "FROM DATE:----\n", this.fromDate)

    if (this.fromDate < this.toDate){
      console.log("FROM DATE  SMALL")
    }else{
      console.log("FROM DATE LARGER")
    }
    if (this.toDate < this.fromDate){
      console.log("TO DATE  SMALL")
    }else{
      console.log("TO DATE LARGER")
    }

    if (this.toDate != null && this.fromDate != null){

      var self = this
      self._customerDebitCreditService.filterDebitCredits(self.selectedCustomer, self.toDate, self.fromDate).then(function(result){
        // console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        self.customerDebitCredits = []
        result.docs.forEach(function(doc){
          self.customerDebitCredits.push(doc)
        })
        // self._customerDebitCreditService.backDatedDebitCredit(self.selectedCustomer, self.fromDate).then(function(result){
        //   // console.log("RESULT FROM BCK DATED 9999++++++++++++ DEBIT CREDIT:--", result)
        // }).catch(function(err){
        //   console.log(err)
        // })
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilter(){
    this.fromDate = null
    this.toDate = null
  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) {
        row.doc.openingbalance = 0 
        row.doc.closingbalance = 0 
        self.allCustomers.push(row.doc); 
      });
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
      // self.debitCreditTotal(self.allCustomers)
      self._customersService.updateCustomers(self.allCustomers).then(function(result){
        // console.log("ALL CUSTOMERS BALANCES SET TO ZERO:=====", result);

      }).catch(function(error) {
        console.log(error)
      })
    }).catch(function(err){
      console.log(err);
    })
  }

  editCustomer(row){
    (<FormGroup>this.editCustomerForm).patchValue({'name':row.name,
                                                  'email':row.email,
                                                  'address':row.address,
                                                  'phone':row.phone,
                                                  'fax':row.fax,
                                                  'obtype':row.obtype,
                                                  'openingbalance':row.opening},{onlySelf:true})
    // row.openingbalance = row.opening
    // console.log("CUSTOMER TO EDIT:-------", row)
    this.editCustomerFlag = true
    this.editCustomerDetails = row
  }

  hideEdit(){
    this.editCustomerFlag = false
    this.loadDataSource()
  }

  udpateCustomerDetail(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = values.openingbalance
        values.status = null
      }
    }
    values._id = this.editCustomerDetails._id
    values._rev = this.editCustomerDetails._rev
    // console.log("EDIT CUSTOMER DETAILS:----", values)
    self._customersService.updateCustomerBalance(values).then(function(result){
      // console.log("RESULT AFTER UPDATING CUSTOMER:---", result)
      self.editCustomerForm.reset()
      // self.loadDataSource()
      self.editCustomerFlag = false
    }).catch(function(err){
      console.log(err)
    })
  }


  debitCreditTotal(customers){
    var debit = 0
    var credit = 0
    for (let item of customers){
      if (item.status === "debit"){
        debit = debit + item.closingbalance
      }
      if (item.status === "credit"){
        credit = credit + item.closingbalance
      }
    }
    this.totalDebits = debit
    this.totalCredits = credit
    // console.log("TOTAL DEBITS:--", this.totalDebits, "\n", "TOTAL CREDITS:--", this.totalCredits, "\n");
  }

  addCustomer(values){
    var self = this;
    if (values.obtype === 'Debit'){
      if (values.openingbalance != 0){
        values.closingbalance = values.openingbalance
        values.status = 'debit'
      }else{
        values.closingbalance = 0
        values.status = null
      }
    }else if (values.obtype === 'Credit'){
      if (values.openingbalance != 0){
        values.openingbalance = -values.openingbalance
        values.closingbalance = values.openingbalance
        values.status = 'credit'
      }else{
        values.closingbalance = 0
        values.status = null
      }
    }
    // console.log("ADD CUSTOMER CALLED:--", values);
    self._customersService.addCustomer(values).then(function(result){
      // console.log("Customer ADDED:===", result);
        self.addCustomerForm.reset();
        self.loadDataSource()
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }



  openLedgerDialog(item): void {
    var self = this
    // console.log("ITEM CLICKED:------", item)
    if (item.dctype === "purchase"){
      self._purchaseService.getSinglePurchase(item.dcrefid).then(function(result){

        // console.log("RESULT FROM SINGLE PURCHASE:--", result);
        let dialogRef = self._dialog.open(customerLedgerDetailDialog, {
          width: '100%',
          height: '600px',
          data: { info: result.docs[0], dcref: 'purchase' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // self.animal = result;
          // console.log('The Ledger detail was closed', result);
          // if (this.animal){
          //   // alert(1)
          //   // console.log('YES TYPED');
          //   this.removeProduct(product)
          // }
        })
      }).catch(function(err){

        console.log(err);
      })
    }
    if (item.dctype === "invoice"){
      self._invoiceService.getSingleInvoice(item.dcrefid).then(function(result){

        // console.log("RESULT FROM SINGLE INVOICE:--", result);
        let dialogRef = self._dialog.open(customerLedgerDetailDialog, {
          width: '100%',
          height: '600px',
          data: { info: result.docs[0], dcref: 'invoice' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // self.animal = result;
          // console.log('The Ledger detail was closed', result);
          // if (this.animal){
          //   // alert(1)
          //   // console.log('YES TYPED');
          //   this.removeProduct(product)
          // }
        })
      }).catch(function(err){

        console.log(err);
      })
    }


  }

  customerDebitCredit(customer){
    var self = this
    self.selectedCustomer = customer
    // console.log("SELCTED CUSTOMER:--- ", self.selectedCustomer)
    // self.syncDebitCredit()
    var sum = 0
    self._customerDebitCreditService.getDebitCredits(customer).then(function(result){
      self.customerDebitCredits = []
      result.docs.forEach(function(doc){
        if (doc.dctype === 'payment'){
          if (doc.paymentdate){

            doc.dcrefdate = doc.paymentdate
          }else{
            doc.dcrefdate = doc._id
          }
        }else if (doc.dctype === 'receipt'){
          if (doc.receiptdate){

            doc.dcrefdate = doc.receiptdate
          }else{
            doc.dcrefdate = doc._id
          }
          // doc.dcrefdate = doc.receiptdate
        }else if (doc.dctype === 'purchase'){
          if (doc.dcrefdate){
            doc.dcrefdate = doc.dcrefdate
          }else{
            doc.dcrefdate = doc._id
          }
        }else{
          if (doc.dcrefdate){
            doc.dcrefdate = doc.dcrefdate
          }else{
            doc.dcrefdate = doc._id
          }
        }
        self.customerDebitCredits.push(doc)
      })
      self.customerDetailFlag = true
      // console.log("RESULT AFTER ALL CUSTOMER DEBIT CREDITS:-----", self.customerDebitCredits)
      // self.customerDebitCredits.sort(function(a,b){
      //   // Turn your strings into dates, and then subtract them
      //   // to get a value that is either negative, positive, or zero.
      //   return b.dcrefdate - a.dcrefdate;
      // });
      self.customerDebitCredits = _.sortBy(self.customerDebitCredits, function(o) { return moment(o.dcrefdate).format('YYYYMMDD'); });
      const users2: UserData2[] = [];
      for (let i=0; i<self.customerDebitCredits.length; i++){
          // console.log("DEBIT CREDIT ENTRY VALUE:---", self.customerDebitCredits[i], customer)
        if (i==0){
          if (self.customerDebitCredits[i].debit != null){
            sum = customer.openingbalance + self.customerDebitCredits[i].debit
          }else{
            sum = customer.openingbalance + self.customerDebitCredits[i].credit
          }
          customer.closingbalance = sum
        }else{
          if (self.customerDebitCredits[i].debit != null){
            sum = customer.closingbalance + self.customerDebitCredits[i].debit
          }else{
            sum = customer.closingbalance + self.customerDebitCredits[i].credit
          }
          customer.closingbalance = sum
        }
        if (customer.closingbalance < 0){
          customer.status = 'credit'
        }else{
          customer.status = 'debit'
        }
        // console.log("CUSTOMER ELSE:---", customer, sum, self.customerDebitCredits[i])
        self.customerDebitCredits[i].closingbalance = sum
        if (self.customerDebitCredits[i].closingbalance < 0){
          self.customerDebitCredits[i].status = 'credit'
        }else{
          self.customerDebitCredits[i].status = 'debit'

        }
        users2.push(createNewUser2(self.customerDebitCredits[i]))
      }
      console.log("CUSTOMER TP UPDATE:---", customer, self.customerDebitCredits)
        // self.selectedCustomer.openingbalance = customer.opening
        // self.selectedCustomer.closingbalance = customer.closing
      // self._customerDebitCreditService.updateDebitCredit(self.customerDebitCredits).then(function(result){
      //   console.log("RESULT AFTER UPDATING DEBIT CREDITS:----", result)
        self._customersService.updateCustomerBalance(self.selectedCustomer).then(function(result){
          // console.log("RESULT AFTER UPDATING CUSTOMER:----", result)
          // self.customerDetailFlag = true
          self.dataSource2 = new MatTableDataSource(users2);
          self.dataSource2.paginator = self.paginator2;
          self.dataSource2.sort = self.sort2;
        }).catch(function(err){
          console.log(err)
        })
      // }).catch(function(err){
      //   console.log(err)
      // })
    }).catch(function(err){
      console.log(err)
    })
  }  




  goToPrint(){
    // this._router.navigate(['customerledger', this.selectedCustomer._id])
    this._electronService.ipcRenderer.send('CustomerLedgerPrint', this.selectedCustomer._id)
  }

  goToPrintCustomers(){
    // this._router.navigate(['printcustomers'])
    this._electronService.ipcRenderer.send('CustomersPrint')
  }

  goBack(){
    if (this.authUser._id === 'admin')
      this._router.navigate(['/admindashboard'])
    else if (this.authUser._id === 'deo')
      this._router.navigate(['/deodashboard'])
  }

  hideCuatomerDetail(){
    this.customerDetailFlag = false
    this.loadDataSource()
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }



  deleteEntry(row): void {
    // var self = this;
    console.log("ENTRY CLICKED:------", row)
    let dialogRef = this._dialog.open(customerLedgerDeleteDialog, {
      width: '250px',
      data: { name: row.refnumber }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
      console.log('The dialog was closed', result, this.animal);
      if (this.animal){
        // alert(1)
        // this.getPurchaseDc(purchase)
        // this.getProductPurchaseDc(purchase)
        // console.log('YES TYPED:--', purchase);
        this.removeEntry(row)
      }
    })
  }

  removeEntry(row){
    var self = this
    row._deleted = true
    console.log("ENTRY TO DELETE:---", row)
    self._customerDebitCreditService.addDebitCredit(row).then(function(result){
      console.log("RESULT AFTER DELETING ENTRY:---", result)
      // self.customerDebitCredit(self.selectedCustomer)
    }).catch(function(err){
      console.log(err)
    })
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  
}

function createNewUser(row): UserData {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    "_id": row._id,
    "_rev": row._rev,
    "name" : row.name,
    "address" : row.address,
    "phone" : row.phone,
    "openingbalance" : row.openingbalance,
    "closingbalance" : row.closingbalance,
    "status" : row.status,
    "fax" : row.fax,
    "obtype" : row.obtype,
    "email" : row.email
  };


}

function createNewUser2(row): UserData2 {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    // "date": row._id,
    // "_rev": row._rev,
    // "name" : row.name,
    // "address" : row.address,
    // "phone" : row.phone,
    // "openingbalance" : row.openingbalance,
    // "closingbalance" : row.closingbalance,
    // "status" : row.status,
    // "fax" : row.fax,
    // "obtype" : row.obtype,
    // "email" : row.email
    refdate: row.dcrefdate ? row.dcrefdate : row._id,
    _id: row._id,
    _rev: row._rev,
    receiptdate: row.receiptdate ? row.receiptdate : null,
    paymentdate: row.paymentdate ? row.paymentdate : null,
    dctype: row.dctype,
    refnumber: row.dcref,
    refvoucherno: row.voucherno ? row.voucherno : row.challanno ? row.challanno : 'NA',
    // refchallanno: row.challanno ? row.challanno : 'NA',
    refbillno: row.billno ? row.billno : 'NA',
    refacc: row.dcrefname ? row.dcrefname : null,
    notes: row.notes ? row.notes : null,
    debit: row.debit,
    credit: row.credit,
    closing: row.closingbalance,
    status: row.status,
    dcrefid: row.dcrefid
  };


}



@Component({
  selector: 'ledger-detail',
  templateUrl: 'ledger-detail.html',
  styleUrls: ['customers.css']
})
export class customerLedgerDetailDialog {

  constructor(
    public dialogRef: MatDialogRef<customerLedgerDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(){
    // console.log("POP UP START DATA:---", this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}


@Component({
  selector: 'customer-ledger-entry-delete',
  templateUrl: 'customer-ledger-entry-delete.html'
})
export class customerLedgerDeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<customerLedgerDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}