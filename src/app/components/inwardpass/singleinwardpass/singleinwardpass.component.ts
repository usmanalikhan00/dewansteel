import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { InwardPassService } from '../../../services/inwardpass.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { ElectronService } from 'ngx-electron'
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'single-inwardpass',
    providers: [LoginService, 
                ElectronService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                InwardPassService],
    templateUrl: 'singleinwardpass.html',
    styleUrls: ['singleinwardpass.css']
})

export class singleinwardpass {
  
  // public productSubCategoriesDB = new PouchDB('productsubcategories');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  inwardpassId: any;
  selectedInwardpass: any;
  purchaseNotes: any;
  authUser: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _inwardPassService: InwardPassService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddSubCategoryForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddSubCategoryForm(){
    // this.addSubCategoryForm = this._formBuilder.group({
    //   name: ['', Validators.required]
    // })
  }
  // ngOnInit() {
  //   this.sub = this._activatedRouter.params.subscribe(params => {
  //      this.categoryId = params['categoryId']; // (+) converts string 'id' to a number
  //   });
  // }
      // this._route.paramMap
      // .switchMap((params: ParamMap) =>
      //   this._demandService.updateDemandStatus(this.demands[i]._id,demState))
      // .subscribe(demands =>{
      //   console.log("Dem Status Resp",demands)
      // });

  ngOnInit(){
    var self = this;

    self.sub = self._activatedRouter.params.subscribe(params => {
      self.inwardpassId = params['inwardpassId']; // (+) converts string 'id' to a number
      console.log("INWARDID TO GET DETAIL:-----", self.inwardpassId)
      self.getSinglePurchase()

    });
  }
  
  getSinglePurchase(){
    var self = this;
    self._inwardPassService.getSingleInwardpass(self.inwardpassId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedInwardpass = result.docs[0]
      console.log("RESULT FROM SELECTED INWARD PASS:---------", self.selectedInwardpass)
    }).catch(function(err){
        console.log(err)
    })

  }

  goToPrint(){
    // this._router.navigate(['printinwardpass', this.selectedInwardpass._id])
    this._electronService.ipcRenderer.send('InwardpassPrint', this.selectedInwardpass._id)

  }


  goBack(){
    this._router.navigate(['purchase'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }


  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
