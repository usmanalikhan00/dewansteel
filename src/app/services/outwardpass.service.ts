import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class OutwardPassService {
  
  authenticatedUser: any = [];
  public outwardPassDB = new PouchDB('steelcustomeroutwardpass');
  

  constructor(private _router: Router){}

  addOutwardpass(outwardpassDoc){
    console.log("OUT WARD PASS DOCUMENT TO PUT:-----", outwardpassDoc)
    var self = this;
    return self.outwardPassDB.put(outwardpassDoc);
  }

  allOutwardpass(){
    var self = this;
    return self.outwardPassDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleOutwardpass(outwardpassId){
    var self = this;
    return self.outwardPassDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.outwardPassDB.find({
        selector: {_id:{$eq: outwardpassId}}
      });
    })
  }

  updateOutwardpass(outwardpassDoc){
    var self = this
    return self.outwardPassDB.put(outwardpassDoc);    
  }


}