import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ElectronService} from 'ngx-electron'
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

export interface UserData {
  purchasenumber ? : string;
  createdby ? : string;
  purchasecustomer ? : string;
  productcount ? : number;
  purchasenotes ? : string;
  purchasetotal ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
}

@Component({
    selector: 'print-purchases',
    providers: [LoginService, ProductService, InvoiceService, PurchaseService, ElectronService],
    templateUrl: 'printpurchases.html',
    styleUrls: ['printpurchases.css']
})

export class printpurchases {
  
  allPurchases: any = [];
  purchasesTotal: any = null;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _electronService: ElectronService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    this.loadDataSource()
  }

  loadDataSource(){
    var self = this;
    // self.allInvoices = [];
    self._purchaseService.allPurchases().then(function(result){
      self.allPurchases = [];
      result.rows.map(function(row){
        self.allPurchases.push(row.doc); 
      })
      console.log("PURCHASES", self.allPurchases)
      self.getPurchasesTotal(self.allPurchases)
      self._electronService.ipcRenderer.send('PrintPurchases')
    }).catch(function(err){
      console.log(err);
    })

  }

  getPurchasesTotal(allPurchases){
    var sum = 0
    for (let doc of allPurchases){
      sum = sum + doc.purchasetotal
    }
    this.purchasesTotal = sum
    // this.invoicesTotal = Number(this.invoicesTotal)
    console.log("SUM FROM PURCHASES TOTAL:===", sum, this.purchasesTotal)
  }

  getSinglePurchase(purchase){
    console.log("Selected Purchase:===", purchase)
    this._router.navigate(['/purchase', purchase._id])
  }

  logout(){
    this._loginService.logout()
  }  
}
