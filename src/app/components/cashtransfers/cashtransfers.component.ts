import {Component, ElementRef, ViewChild, HostListener, } from '@angular/core';
import {LoginService} from '../../services/login.service'
import {SettingsService} from '../../services/settings.service'
import {CashTransferService} from '../../services/cashtransfers.service'
import {AccountDebitCreditService} from '../../services/accountdebitcredit.service'
import { AccountTypesService } from '../../services/accounttypes.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { UUID } from 'angular2-uuid';

export interface CashTransfers {
  trasnferdate: string;
  transfernumber: string;
  fromaccount: string;
  toaccount: string;
  voucherno: string;
  transfernotes: string;
  _id: string;
  amount: number;
}

// const ELEMENT_DATA: CashTransfers[] = [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
// ];

@Component({
    selector: 'cash-transfers',
    providers: [LoginService, SettingsService, AccountTypesService, CashTransferService, AccountDebitCreditService],
    templateUrl: 'cashtransfers.html',
    styleUrls: ['cashtransfers.css']
})


export class cashtransfers {
  

  transferForm: FormGroup;
  authUser: any = null
  transferdate: any = null
  alltransfers: any = []

  fromSelectedAccountTypes: any = []
  selectedFromAccount: any = null
  fromAccountToUpdate: any = null
  fromAccountTypeChangeValue: any = null
  fromAccountDebitCredit: any = null
  fromAccountStates: Observable<any[]>;
  
  toSelectedAccountTypes: any = []
  selectedToAccount: any = null
  toAccountToUpdate: any = null
  toAccountTypeChangeValue: any = null
  toAccountDebitCredit: any = null
  toAccountStates: Observable<any[]>;

  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  displayedColumns = ['transferdate', 'transfernumber', 'voucher', 'transfer', 'fromaccount', 'toaccount', 'amount'];
  dataSource: MatTableDataSource<CashTransfers>;

  transferFlag: boolean = false

  allFromAccTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
  ]

  allToAccTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
  ]

  transferNumber: any = null

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  public cashTransfersDB = new PouchDB('steelcashtransfers');
  public cloudantCashTransfersDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewancashtransfers', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });


  constructor(private _loginService: LoginService,
              private _settingsService: SettingsService, 
              private _accountTypesService: AccountTypesService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _cashTransferService: CashTransferService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildTransferForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
    // this.dataSource = new MatTableDataSource(ELEMENT_DATA)
  }

  private _buildTransferForm(){
    this.transferForm = this._formBuilder.group({
      from: ['', Validators.required],
      fromaccount: ['', Validators.required],
      to: ['', Validators.required],
      toaccount: ['', Validators.required],
      amount: ['', Validators.required],
      voucherno: [''],
      transfernotes: [''],
      transferdate: [null],
    })
  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.transferNumber = Math.floor(Math.random()*1000) + 100000
    this.loadDataSource()
    // this.syncDb()
  }


  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.cashTransfersDB.replicate.from(self.cloudantCashTransfersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM ONE-WAY TRASNSFERS REPLICATION:--", info)
      self.cashTransfersDB.sync(self.cloudantCashTransfersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY TRASNSFERS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.loadDataSource()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC TRASNSFERS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY TRASNSFERS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }


  loadDataSource(){
    var self = this;
    self._cashTransferService.allCashTransfers().then(function(result){
      // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
      // console.log("ALL INVOICES:=====", self.allInvoices);
      const transfers: CashTransfers[] = [];
      self.alltransfers = [];
      result.rows.map(function(row){
        transfers.push(createNewTransfer(row.doc))
        self.alltransfers.push(row.doc)
      })
      self.dataSource = new MatTableDataSource(transfers);
      self.dataSource.paginator = self.paginator;
      self.dataSource.sort = self.sort;
      // self.getInvoicesTotal(self.allInvoices)
      // console.log("ALL INVOCIES ON NGONINIT:===", self.allInvoices)
    }).catch(function(err){
      console.log(err);
    })

  }  

  addTransfer(values){

    var self = this
    console.log("VALUES SUBMITTED FROM FORM:---", values, 
                "\nTo Account:----", this.toAccountToUpdate,
                "\nTo Account Type:----", this.toAccountTypeChangeValue,
                "\nFrom Account:----", this.fromAccountToUpdate,
                "\nFrom Account Type:----", this.fromAccountTypeChangeValue,
                )


    var accountDebitCreditsArray: any = []
    var updatedAccountsArray: any = []
    var transferObject = null

    var things = ['a', 'b', 'c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r'];
    var thing = things[Math.floor(Math.random()*things.length)];

    if (values.transferdate === null){
      transferObject = {
        _id: new Date().toISOString(),
        transfernumber: self.transferNumber,
        fromaccount: self.fromAccountToUpdate[0],
        toaccount: self.toAccountToUpdate[0],
        trasnferdate: values.transferdate,
        transfernotes: values.transfernotes,
        voucherno: values.voucherno,
        amount: values.amount,
      }
      
    }else{
      transferObject = {
        _id: new Date().toISOString(),
        transfernumber: self.transferNumber,
        fromaccount: self.fromAccountToUpdate[0],
        toaccount: self.toAccountToUpdate[0],
        trasnferdate: values.transferdate.toISOString(),
        transfernotes: values.transfernotes,
        voucherno: values.voucherno,
        amount: values.amount,
      }
    }

    console.log("TRASNFER OBJECT FOR DATABASE***********:========", transferObject)

    // FROM ACCOUNT DETAILS FOR UPDATE
    self.fromAccountToUpdate[0].closingbalance = self.fromAccountToUpdate[0].closingbalance - values.amount
    self.fromAccountDebitCredit = {
      _id: new Date().toISOString(),
      accountid: self.fromAccountToUpdate[0]._id,
      accountnumber: self.fromAccountToUpdate[0].accountnumber,
      accountrev: self.fromAccountToUpdate[0]._rev,
      accounttype: self.fromAccountToUpdate[0].accounttype,
      openingbalance: self.fromAccountToUpdate[0].openingbalance,
      closingbalance: self.fromAccountToUpdate[0].closingbalance,
      customer: self.toAccountToUpdate[0],
      customerid: self.toAccountToUpdate[0]._id,
      dcref: transferObject.transfernumber,
      dcrefid: transferObject._id,
      transferdate: values.transferdate,
      transfernotes: values.transfernotes,
      voucherno: values.voucherno,
      dctype: "transfer",            
      obtype: self.fromAccountToUpdate[0].obtype
    }
    self.fromAccountDebitCredit.debit = null

    if (self.fromAccountToUpdate[0].closingbalance > 0){
      self.fromAccountToUpdate[0].status = 'debit'
      self.fromAccountDebitCredit.status = 'debit'
      self.fromAccountDebitCredit.credit = -values.amount
    }else if (self.fromAccountToUpdate[0].closingbalance < 0){
      self.fromAccountToUpdate[0].status = 'credit'
      self.fromAccountDebitCredit.status = 'credit'
      self.fromAccountDebitCredit.credit = -values.amount
    }
    console.log("FROM ACCOUNT DEBIT CREDIT OBJECT:========", self.fromAccountDebitCredit)
    console.log("ACTUAL FROM ACCOUNT TO UPDATE:========", self.fromAccountToUpdate[0])

    // TO ACCOUNT DETAILS FOR UPDATE
    self.toAccountToUpdate[0].closingbalance = self.toAccountToUpdate[0].closingbalance + values.amount
    self.toAccountDebitCredit = {
      _id:UUID.UUID().substr(28, 35),
      accountid:self.toAccountToUpdate[0]._id,
      accountnumber:self.toAccountToUpdate[0].accountnumber,
      accountrev:self.toAccountToUpdate[0]._rev,
      accounttype:self.toAccountToUpdate[0].accounttype,
      openingbalance:self.toAccountToUpdate[0].openingbalance,
      closingbalance:self.toAccountToUpdate[0].closingbalance,
      customer:self.fromAccountToUpdate[0],
      customerid:self.fromAccountToUpdate[0]._id,
      dcref: transferObject.transfernumber,
      dcrefid: transferObject._id,
      transferdate: values.transferdate,
      transfernotes: values.transfernotes,
      voucherno: values.voucherno,
      dctype: "transfer",            
      obtype:self.toAccountToUpdate[0].obtype
    }
    self.toAccountDebitCredit.credit = null

    if (self.toAccountToUpdate[0].closingbalance > 0){
      self.toAccountToUpdate[0].status = 'debit'
      self.toAccountDebitCredit.status = 'debit'
      self.toAccountDebitCredit.debit = values.amount
    }else if (self.toAccountToUpdate[0].closingbalance < 0){
      self.toAccountToUpdate[0].status = 'credit'
      self.toAccountDebitCredit.status = 'credit'
      self.toAccountDebitCredit.debit = values.amount
    }
    console.log("TO ACCOUNT DEBIT CREDIT OBJECT:========", self.toAccountDebitCredit)
    console.log("ACTUAL TO ACCOUNT TO UPDATE:========", self.toAccountToUpdate[0])


    accountDebitCreditsArray.push(self.fromAccountDebitCredit)
    accountDebitCreditsArray.push(self.toAccountDebitCredit)

    updatedAccountsArray.push(self.fromAccountToUpdate[0])
    updatedAccountsArray.push(self.toAccountToUpdate[0])

    self._cashTransferService.addCashTransfer(transferObject).then(function(result){
      console.log("RESULT AFTER ADDING TRANSFER:----", result)
      self._accountDebitCreditService.addDebitCredit(self.fromAccountDebitCredit).then(function(result){
        console.log("RESULT AFTER ADDING FROM DEBIT CREDITS:------", result)
        self._accountDebitCreditService.addDebitCredit(self.toAccountDebitCredit).then(function(result){
          console.log("RESULT AFTER ADDING TO DEBIT CREDITS:------", result)
          self._accountTypesService.updateAccountBalance(self.fromAccountToUpdate[0]).then(function(result){
            console.log("RESULT AFTER UPDATE ACTUAL FROM ACCOUNTS:------", result)
            self._accountTypesService.updateAccountBalance(self.toAccountToUpdate[0]).then(function(result){
              console.log("RESULT AFTER UPDATE ACTUAL TO ACCOUNTS:------", result)
              self.transferForm.reset()
              self.selectedFromAccount = null
              self.selectedToAccount = null
              self.fromAccountToUpdate = null
              self.toAccountToUpdate = null
              self.transferNumber = Math.floor(Math.random()*1000) + 100000
            }).catch(function(err){
              console.log(err)
            })

          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })


  }


  checkEntries(row){
    var self = this
    console.log("ENTRIES TO CHECK:---", row)

    self._cashTransferService.getSingleTrasnfer(row._id).then(function(result) {
      var transfer = result.docs[0]
      console.log("ACTUAL TRANFER:--", transfer)
      self._accountDebitCreditService.getSingleDebitCredit(transfer._id).then(function(result){
        console.log("RESULT FROM GET DEBIT CREDIT:--", result)
        var toAccountDebitCredit: any = null
        var toAccountToUpdate: any = null
        if (result.docs.length <= 1){
          console.log("SINGLE ENTRY:--")
          if (result.docs[0].accountid === transfer.toaccount._id){
            console.log("TO ACCOUNT DETECTED:--")
          }
          if (result.docs[0].accountid === transfer.fromaccount._id){
            console.log("FROM ACCOUNT DETECTED:--", transfer.toaccount._id)
            self._accountTypesService.getSingleAccount(transfer.toaccount._id).then(function(result){
              toAccountToUpdate = result.docs[0]
              console.log("TO ACCOUNT FOR DATABASE:--", toAccountToUpdate)
              // TO ACCOUNT DETAILS FOR UPDATE
              toAccountToUpdate.closingbalance = toAccountToUpdate.closingbalance + transfer.amount
              toAccountDebitCredit = {
                _id:new Date().toISOString(),
                accountid: toAccountToUpdate._id,
                accountnumber: toAccountToUpdate.accountnumber,
                accountrev: toAccountToUpdate._rev,
                accounttype: toAccountToUpdate.accounttype,
                openingbalance: toAccountToUpdate.openingbalance,
                closingbalance: toAccountToUpdate.closingbalance,
                customer: transfer.fromaccount,
                customerid: transfer.fromaccount._id,
                dcref: transfer.transfernumber,
                dcrefid: transfer._id,
                transferdate: transfer.trasnferdate,
                transfernotes: transfer.transfernotes,
                voucherno: transfer.voucherno,
                dctype: "transfer",            
                obtype: toAccountToUpdate.obtype
              }
              toAccountDebitCredit.credit = null

              if (toAccountToUpdate.closingbalance > 0){
                toAccountToUpdate.status = 'debit'
                toAccountDebitCredit.status = 'debit'
                toAccountDebitCredit.debit = transfer.amount
              }else if (toAccountToUpdate.closingbalance < 0){
                toAccountToUpdate.status = 'credit'
                toAccountDebitCredit.status = 'credit'
                toAccountDebitCredit.debit = transfer.amount
              }
              console.log("TO ACCOUNT DEBIT CREDIT OBJECT:========", toAccountDebitCredit)
              console.log("ACTUAL TO ACCOUNT TO UPDATE:========", toAccountToUpdate)
              self._accountDebitCreditService.addDebitCredit(toAccountDebitCredit).then(function(result){
                console.log("RESULT AFTER ADDING DEBIT/CREDIT:---", result)
                self._accountTypesService.updateAccountBalance(toAccountToUpdate).then(function(result){
                  console.log("RESULT AFTER UPDATING TO ACCCOUNT:---", result)
                }).catch(function(err){
                  console.log(err)
                })
              }).catch(function(err){
                console.log(err)
              })
            }).catch(function(err) {
              console.log(err)
            })
          }
        }else if  (result.docs.length === 2){
          console.log("DOUBLE ENTRY:--")
        }
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  transferDateChange(e){
    // console.log("TRANSEFR DATE CHANGES:---", e.value)
    this.transferdate = e.value.toISOString()
    // console.log("DATE TO ENTER:---", this.transferdate)
  }

  fromAccountTypeChange(value){
    (<FormGroup>this.transferForm).patchValue({'fromaccount':''}, { onlySelf: true });
    var self = this
    // console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    self.fromAccountTypeChangeValue = value
    self.fromSelectedAccountTypes = []
    self._accountTypesService.getAccountSubAccounts(value).then(function(result){
      console.log("RESULT FROM SINGLE ACCOUNT ENTRIES:=======", result)
      result.docs.forEach(function(row){
        self.fromSelectedAccountTypes.push(row)
      })
      self.fromAccountStates = self.transferForm.controls['fromaccount'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterFromAccounts(state) : self.fromSelectedAccountTypes.slice());
      console.log("ALL ACCOUNTS OF SELECTED TYPE:=======", self.fromSelectedAccountTypes)
    }).catch(function(err){
      console.log(err)
    })
  }

  selectFromAccount(state){
    this.selectedFromAccount = state
    this.fromAccountToUpdate = _.filter(this.fromSelectedAccountTypes, {"_id": this.selectedFromAccount._id})
    this.fromAccountTypeChange(this.fromAccountTypeChangeValue)

  }

  toAccountTypeChange(value){
    (<FormGroup>this.transferForm).patchValue({'toaccount':''}, { onlySelf: true });
    var self = this
    // console.log("VALUES FROM ACCOUNT CHANGE:-----", value)
    self.toAccountTypeChangeValue = value
    self.toSelectedAccountTypes = []
    self._accountTypesService.getAccountSubAccounts(value).then(function(result){
      console.log("RESULT FROM SINGLE ACCOUNT ENTRIES:=======", result)
      result.docs.forEach(function(row){
        self.toSelectedAccountTypes.push(row)
      })
      self.toAccountStates = self.transferForm.controls['toaccount'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterToAccounts(state) : self.toSelectedAccountTypes.slice());
      console.log("ALL ACCOUNTS OF SELECTED TYPE:=======", self.toSelectedAccountTypes)
    }).catch(function(err){
      console.log(err)
    })
  }

  selectToAccount(state){
    this.selectedToAccount = state
    this.toAccountToUpdate = _.filter(this.toSelectedAccountTypes, {"_id": this.selectedToAccount._id})
    this.toAccountTypeChange(this.toAccountTypeChangeValue)
  }



  goBack(){
    this.transferFlag = false;
    this.loadDataSource()
  }

  goToEdit(){
    this.transferFlag = true;
  }


  filterFromAccounts(name: string) {
    console.log("ALL ACCOUNTS FROM FILTER:=====", this.transferForm.controls['fromaccount'].value);
    return this.fromSelectedAccountTypes.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterToAccounts(name: string) {
    console.log("ALL ACCOUNTS FROM FILTER:=====", this.transferForm.controls['toaccount'].value);
    return this.toSelectedAccountTypes.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }



  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  

}

function createNewTransfer(row): CashTransfers {

  // console.log("ROW TO SHOW FOR TRANSFERS", row)

  return {
    "transfernumber": row.transfernumber,
    "trasnferdate": row.trasnferdate,
    "toaccount": row.toaccount.name,
    "fromaccount" : row.fromaccount.name,
    "transfernotes" : row.transfernotes,
    "voucherno" : row.voucherno,
    "amount" : row.amount,
    "_id" : row._id,
  };
}