import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { Router, ActivatedRoute } from  '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as _ from 'lodash'
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import { MatSidenav  } from "@angular/material";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { ElectronService } from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;
import * as moment from "moment";
import * as _ from "lodash";

export interface UserData2 {
  refdate ? : string;
  dctype ? : string;
  refnumber ? : string;
  refvoucherno ? : string;
  refchallanno ? : string;
  refbillno ? : string;
  receiptdate ? : string;
  paymentdate ? : string;
  refacc ? : string;
  notes ? : string;
  debit ? : number;
  credit ? : number;
  closing ? : number;
  status ? : string;
  dcrefid ? : string;
  _id ? : string;
  _rev ? : string;

}


@Component({
    selector: 'customer-ledger',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                CustomersService, 
                ElectronService, 
                MediaMatcher, 
                CustomerDebitCreditService],
    templateUrl: 'customerledger.html',
    styleUrls: ['customerledger.css']
})

export class customerledger {
  

  allCustomers: any = []
  customersToUpdate: any = []
  productTotalStock: any = 0
  itemDetailFlag: boolean = false

  balanceTypes = ['Credit', 'Debit']

  allDebitCredits: any = null
  customerId: any = null
  sub: any = null
  selectedCustomer: any = null

  toDate: any = null
  fromDate: any = null


  displayedColumns2 = [
    'date',
    'type', 
    'refnumber', 
    'voucherno', 
    'billno', 
    "refacc",
    "notes",
    'debit',
    'credit',
    'closing',
    'status',
  ];

  dataSource2: MatTableDataSource<UserData2>;

  @ViewChild(MatPaginator) paginator2: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;


  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _customersService: CustomersService, 
              private _electronService: ElectronService, 
              private _activatedRoute: ActivatedRoute, 
              private _cdr: ChangeDetectorRef, 
              private _media: MediaMatcher, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    // this.mobileQuery = _media.matchMedia('(max-width: 600px)');
    // this._mobileQueryListener = () => _cdr.detectChanges();
    // this.mobileQuery.addListener(this._mobileQueryListener);
  }



  ngOnInit(){
    var self = this;
    var sum = 0
    self.sub = self._activatedRoute.params.subscribe(params => {
      self.customerId = params['customerId']; // (+) converts string 'id' to a number
      // console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.productId)
      self._customersService.getSingleCustomer(self.customerId).then(function(result){
        console.log("SINGLE CUSTOMER:====", result)
        self.selectedCustomer = result.docs[0]
        self.allDebitCredits =[]
        self._customerDebitCreditService.printDebitCredits(result.docs[0]).then(function(result){
          // console.log("RESULT FROM SINGLE CUSTOMER DEBTI CREDIT:====", result)
          result.docs.forEach(function(doc){
            if (doc.dctype === 'payment'){
              if (doc.paymentdate){

                doc.dcrefdate = doc.paymentdate
              }else{
                doc.dcrefdate = doc._id
              }

            }else if (doc.dctype === 'receipt'){
              if (doc.receiptdate){

                doc.dcrefdate = doc.receiptdate
              }else{
                doc.dcrefdate = doc._id
              }

            }else if (doc.dctype === 'purchase'){
              if (doc.dcrefdate){
                doc.dcrefdate = doc.dcrefdate
              }else{
                doc.dcrefdate = doc._id
              }
            }else{
              if (doc.dcrefdate){
                doc.dcrefdate = doc.dcrefdate
              }else{
                doc.dcrefdate = doc._id
              }

            }
            self.allDebitCredits.push(doc)
          })

          // console.log("ALL DEBIT CREDITS TO PRINT:------", self.allDebitCredits)
          self.allDebitCredits = _.sortBy(self.allDebitCredits, function(o) { return moment(o.dcrefdate).format('YYYYMMDD'); });
          const users2: UserData2[] = [];
          for (let i=0; i<self.allDebitCredits.length; i++){
            // console.log("DEBIT CREDIT ENTRY VALUE:---", self.customerDebitCredits[i], customer)
            if (i==0){
              if (self.allDebitCredits[i].debit != null){
                sum = self.selectedCustomer.openingbalance + self.allDebitCredits[i].debit
              }else{
                sum = self.selectedCustomer.openingbalance + self.allDebitCredits[i].credit
              }
              self.selectedCustomer.closingbalance = sum
            }else{
              if (self.allDebitCredits[i].debit != null){
                sum = self.selectedCustomer.closingbalance + self.allDebitCredits[i].debit
              }else{
                sum = self.selectedCustomer.closingbalance + self.allDebitCredits[i].credit
              }
              self.selectedCustomer.closingbalance = sum
            }
            if (self.selectedCustomer.closingbalance < 0){
              self.selectedCustomer.status = 'credit'
            }else{
              self.selectedCustomer.status = 'debit'
            }

            // console.log("CUSTOMER ELSE:---", self.selectedCustomer, sum, self.allDebitCredits[i])
            self.allDebitCredits[i].closingbalance = sum
            if (self.allDebitCredits[i].closingbalance < 0){
              self.allDebitCredits[i].status = 'credit'
            }else{
              self.allDebitCredits[i].status = 'debit'
            }
            users2.push(createNewUser2(self.allDebitCredits[i]))
          }
          // console.log("CUSTOMER TP UPDATE:---", self.selectedCustomer, self.allDebitCredits)
          self.dataSource2 = new MatTableDataSource(users2);
          self._electronService.ipcRenderer.send('PrintCustomerLedger')
          // self.dataSource2.paginator = self.paginator2;
          // self.dataSource2.sort = self.sort2;
        }).catch(function(err){
            console.log(err)
        })
      }).catch(function(err){
          console.log(err)

      })
    })
  }


  logout(){
    this._loginService.logout()
  }

  ngOnDestroy(): void {
    // this.mobileQuery.removeListener(this._mobileQueryListener);
  }  

}

function createNewUser2(row): UserData2 {
  // const name =
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
  //     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    // "date": row._id,
    // "_rev": row._rev,
    // "name" : row.name,
    // "address" : row.address,
    // "phone" : row.phone,
    // "openingbalance" : row.openingbalance,
    // "closingbalance" : row.closingbalance,
    // "status" : row.status,
    // "fax" : row.fax,
    // "obtype" : row.obtype,
    // "email" : row.email
    refdate: row.dcrefdate ? row.dcrefdate : row._id,
    _id: row._id,
    _rev: row._rev,
    receiptdate: row.receiptdate ? row.receiptdate : null,
    paymentdate: row.paymentdate ? row.paymentdate : null,
    dctype: row.dctype,
    refnumber: row.dcref,
    refvoucherno: row.voucherno ? row.voucherno : row.challanno ? row.challanno : 'NA',
    // refchallanno: row.challanno ? row.challanno : 'NA',
    refbillno: row.billno ? row.billno : 'NA',
    refacc: row.dcrefname ? row.dcrefname : null,
    notes: row.notes ? row.notes : null,
    debit: row.debit,
    credit: row.credit,
    closing: row.closingbalance,
    status: row.status,
    dcrefid: row.dcrefid
  };


}
