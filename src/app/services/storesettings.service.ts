import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class StoreSettingsService {
  
  public storesDB = new PouchDB('steelstores');


  constructor(private _router: Router){}

  addStore(values){
    var self = this;
    return self.storesDB.put({
      _id: new Date().toISOString(),
      name: values.name
    });
  }

  allStores(){
    var self = this;
    return self.storesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }
  


  getSingleStore(customerId){
    var self = this;
    return self.storesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.storesDB.find({
        selector: {_id:{$eq: customerId}}
      });
    })
  }


}