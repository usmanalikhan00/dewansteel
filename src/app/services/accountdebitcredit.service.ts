import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class AccountDebitCreditService {
  
  authenticatedUser: any = [];
  public accountDebitCreditDB = new PouchDB('steelaccountdebitcredit');
  

  constructor(private _router: Router){}

  addDebitCredit(account){
    var self = this;
    // console.log("ACCOUNT FOR DEBIT CREDIT:-----", account)
    return self.accountDebitCreditDB.put(account)
  }

  allDebitCredits(){
    var self = this;
    // console.log("ACCOUNT FOR DEBIT CREDIT:-----", account)
    return self.accountDebitCreditDB.allDocs({
      include_docs: true,
      attachments: true,
      // descending: true,
      // 'startkey':'_design',
      'endKey':'_design',
    })
  }



  addBulkDebitCredit(objects){
    var self = this;
    // console.log("----:DEBIT CREDIT BULK DOCS FROM SERVICE:-----", objects)
    return self.accountDebitCreditDB.bulkDocs(objects)
  }


  
  getDebitCredits(account){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['accountid']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          accountid:{
            $eq:account._id
          }
        }
      });
    })
  }

  getTransferDebitCredits(){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"transfer"
          }
        }
      });
    })
  }

  getPaymentDebitCredits(){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"payment"
          }
        }
      });
    })
  }

  getReceiptDebitCredits(){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['dctype']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          dctype:{
            $eq:"receipt"
          }
        }
      });
    })
  }





  getSingleDebitCredit(id){
    var self = this;
    return self.accountDebitCreditDB.createIndex({
      index: {
        fields: ['dcrefid']
      }
    }).then(function(){
      return self.accountDebitCreditDB.find({
        selector: {
          dcrefid:{
            $eq:id
          }
        }
      });
    })
  }





}