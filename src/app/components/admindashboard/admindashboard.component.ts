import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../services/login.service'
import {InvoiceService} from '../../services/invoice.service'
import {PurchaseService} from '../../services/purchase.service'
import {ProductService} from '../../services/product.service'
// import { User } from '../../models/model-index'
import { AccountTypesService } from '../../services/accounttypes.service'
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { AccountDebitCreditService } from '../../services/accountdebitcredit.service'
import { CashTransferService } from '../../services/cashtransfers.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib';
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

import * as _ from 'lodash'

@Component({
    selector: 'admin-dashboard',
    providers: [LoginService,
                InvoiceService,
                PurchaseService,
                ProductService,
                CustomerDebitCreditService,
                AccountDebitCreditService,
                CashTransferService,
                AccountTypesService],
    templateUrl: 'admindashboard.html',
    styleUrls: ['admindashboard.css']
})

export class admindashboard {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  users: any = [];
  allAccounts: any = [];
  allProducts: any = [];
  allInvoices: any = [];
  allInvoicesDC: any = [];
  allPurchases: any = [];
  allPurchasesDC: any = [];
  allAccountsDC: any = [];
  totalStockValue: any = 0
  totalInvoice: any = 0
  totalPurchases: any = 0

  addProductForm: FormGroup;
  authUser: any = null
  allTransfers: any = []

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _productService: ProductService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _accountDebitCreditService: AccountDebitCreditService, 
              private _accountTypesService: AccountTypesService, 
              private _cashTransferService: CashTransferService, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){
    
  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
    this.authUser = JSON.parse(localStorage.getItem('user'))
    console.log("AUTH USER:-------", this.authUser)
    this.getAllAccounts()
    this.profitLossAccount()
    // this.getCustomerInvoiceDC()
    this.getAccountsTransferDC()
    this.getAllTransfers()
    // this.getCustomerPurchaseDC()
  }


  getCustomerInvoiceDC(){
    var self = this
    var invoiceDC = 0
    self._customerDebitCreditService.getInvoiceDebitCredits().then(function(result){
      self.allInvoicesDC = result.docs
      for (let obj of self.allInvoicesDC){
        var dateKey = _.filter(self.allInvoices, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].invoicedate
        if (dateKey[0])
          invoiceDC++
      }
      console.log("ALL DEBIT/CREDIT FOR INVOICE:-----", self.allInvoicesDC)
      console.log("UPDATED INVOICE COUNT:-----", invoiceDC)
      // self.updateInvoiceDC(self.allInvoicesDC)
    }).catch(function(err){
      console.log(err)
    })
  }

  getAllTransfers(){
    var self = this
    var invoiceDC = 0
    self._cashTransferService.allCashTransfers().then(function(result){
      // console.log("ALL TRASNSFERS:-----", result)
      // self.allTransfers = result.rows
      self.allTransfers = []
      result.rows.map(function (row){
        self.allTransfers.push(row.doc)
      })
      console.log("ALL TRASNSFERS:-----", self.allTransfers)
    }).catch(function(err){
      console.log(err)
    })
  }


  getAccountsTransferDC(){
    var self = this
    var invoiceDC = 0
    self._accountDebitCreditService.getTransferDebitCredits().then(function(result){
      // console.log("ALL TRASNSFER DEBIT/CREDIT FROM ACCOUNTS:-----", result)
      self.allAccountsDC = result.docs
      console.log("ALL DEBIT/CREDIT FOR ACCOUNTS:-----", self.allAccountsDC)
      self.checkTransferDc()
    }).catch(function(err){
      console.log(err)
    })
  }

  checkTransferDc(){
      var transfercount = 0
    for (let transfer of this.allTransfers){
      var count = 0
      var toAccountToUpdate = null
      var fromAccountToUpdate = null
      var fromAccountDebitCredit = null
      var toAccountDebitCredit = null
      var toaccount = false
      var fromaccount = false
      for (let obj of this.allAccountsDC){
        if (transfer._id === obj.dcrefid){
          count++
          if (transfer.fromaccount._id === obj.accountid){
            // console.log("FROM ACCOUNT DETECTED...")
            fromaccount = true
          }
          if (transfer.toaccount._id === obj.accountid){
            // console.log("TO ACCOUNT DETECTED...")
            toaccount = true
          }
        }
      }
          transfercount++
      // console.log('COUNT FOR THIS TRANSFER:---', count, 
      //             "\nFROM ACCOUNT DETCTED:---", fromaccount,
      //             "\nTO ACCOUNT DETCTED:---", toaccount, 
      //             "\nTRANSFER OBJECT:---", transfer)
      // if (fromaccount == false){
      //   var self = this
      //   self._accountTypesService.getSingleAccount(transfer.fromaccount._id).then(function(result){
      //     fromAccountToUpdate = result.docs[0]
      //     // FROM ACCOUNT DETAILS FOR UPDATE
      //     fromAccountToUpdate.closingbalance = fromAccountToUpdate.closingbalance - transfer.amount
      //     fromAccountDebitCredit = {
      //       _id: new Date().toISOString(),
      //       accountid: fromAccountToUpdate._id,
      //       accountnumber: fromAccountToUpdate.accountnumber,
      //       accountrev: fromAccountToUpdate._rev,
      //       accounttype: fromAccountToUpdate.accounttype,
      //       openingbalance: fromAccountToUpdate.openingbalance,
      //       closingbalance: fromAccountToUpdate.closingbalance,
      //       customer: transfer.toaccount,
      //       customerid: transfer.toaccount._id,
      //       dcref: transfer.transfernumber,
      //       dcrefid: transfer._id,
      //       transferdate: transfer.transferdate,
      //       transfernotes: transfer.transfernotes,
      //       voucherno: transfer.voucherno,
      //       dctype: "transfer",            
      //       obtype: fromAccountToUpdate.obtype
      //     }
      //     fromAccountDebitCredit.debit = null

      //     if (fromAccountToUpdate.closingbalance > 0){
      //       fromAccountToUpdate.status = 'debit'
      //       fromAccountDebitCredit.status = 'debit'
      //       fromAccountDebitCredit.credit = -transfer.amount
      //     }else if (fromAccountToUpdate.closingbalance < 0){
      //       fromAccountToUpdate.status = 'credit'
      //       fromAccountDebitCredit.status = 'credit'
      //       fromAccountDebitCredit.credit = -transfer.amount
      //     }
      //     console.log("FROM ACCOUNT DEBIT CREDIT OBJECT:========", fromAccountDebitCredit)
      //     console.log("ACTUAL FROM ACCOUNT TO UPDATE:========", fromAccountToUpdate)
      //     // console.log("FROM ACCOUNT TO UPDATE:---", accountfrom)
      //   }).catch(function(err){
      //     console.log(err)
      //   })
      // }
      // if (toaccount == false){
      //   var self = this
      //   self._accountTypesService.getSingleAccount(transfer.toaccount._id).then(function(result){
      //     toAccountToUpdate = result.docs[0]
      //     console.log("TO ACCOUNT TO UPDATE:---", toAccountToUpdate, "\nActual Trasnfer:---", transfer)
      //   }).catch(function(err){
      //     console.log(err)
      //   })
      // }
    }
    console.log("TRANFERS COUNT:--", transfercount)
    // var zeroCount = 0
    // var oneCount = 0
    // var twoCount = 0
    // for (let transfer of this.allTransfers){
    //   var key = _.filter(this.allAccountsDC, {dcrefid: transfer._id})
    //   if (key){
    //     console.log("TRANSFER FOUND:---", key, key.length, transfer)
    //     if (key.length == 0)
    //       zeroCount++
    //     if (key.length == 1)
    //       oneCount++
    //     if (key.length == 2)
    //       twoCount++

    //   }
    // }
    // console.log("COUNTS FOR DC COUNT 0:---", zeroCount,
    //             "\nCOUNTS FOR DC COUNT 1:---", oneCount,
    //             "\nCOUNTS FOR DC COUNT 2:---", twoCount,
    //             )

  }



  updateInvoiceDC(allInvoicesDC){
    var self = this
    self._customerDebitCreditService.updateDebitCredit(allInvoicesDC).then(function(result){
      console.log("RESULT AFTER UPDATING ALL INVOICES DEBIT/CREDIT:---", result)
    }).then(function(err){
      console.log(err)
    })
  }

  getCustomerPurchaseDC(){
    var self = this
    var purchaseDC = 0
    self._customerDebitCreditService.getPurchaseDebitCredits().then(function(result){
      self.allPurchasesDC = result.docs
      for (let obj of self.allPurchasesDC){
        var dateKey = _.filter(self.allPurchases, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].purchasedate
        if (dateKey[0])
          purchaseDC++
      }
      console.log("ALL DEBIT/CREDIT FOR PUCRHASE:-----", self.allPurchasesDC)
      console.log("UPDATED PURCHASE COUNT:-----", purchaseDC)
      // self.updateInvoiceDC(self.allPurchasesDC)
    }).catch(function(err){
      console.log(err)
    })


  }

  getAllAccounts(){
    var self = this
    self._accountTypesService.getPendingAccounts().then(function(result){
      // console.log("ALL PENDING ACCOUNTS:--------", result)
      self.allAccounts = []
      result.docs.forEach(function(row){
        // if ()
        let diffLeadtime = moment(row.expirydate).diff(moment().format('YYYY-MM-DD'),'days');
        // console.log("DIFF IN LEAD TIME:--", diffLeadtime)
        row.notice = diffLeadtime
        self.allAccounts.push(row)
        // console.log("ALL ACCOUNTS AFTER DIFFERENCE WITH CURRENT TIME:--------", self.allAccounts)
      })
    }).catch(function(err) {
      console.log(err)
    })
  }

  profitLossAccount(){
    var self = this
    self._productService.allProducts().then(function(result){
      var stockSum = 0
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc)
        stockSum = stockSum + row.doc.networth 
      });
      self.totalStockValue = stockSum
      // console.log("ALL PRODCTS * SUM:--", self.allProducts, self.totalStockValue)
      self._invoiceService.allInvocies().then(function(result){
        var stockSum = 0
        result.rows.map(function (row) { 
          self.allInvoices.push(row.doc); 
          stockSum = stockSum + row.doc.invoicetotal 
        });
        self.totalInvoice = stockSum
        // console.log("ALL INVOICES:--", self.allInvoices, self.totalInvoice)
        self._purchaseService.allPurchases().then(function(result){
          var stockSum = 0
          result.rows.map(function (row) { 
            self.allPurchases.push(row.doc); 
            stockSum = stockSum + row.doc.purchasetotal 
          });
          self.totalPurchases = stockSum
          // console.log("ALL PURCHASES:--", self.allPurchases, self.totalPurchases)
        }).catch(function(err) {
          console.log(err)
        })
      }).catch(function(err) {
        console.log(err)
      })    
    }).catch(function(err) {
      console.log(err)
    })
  }

  goToAccounts(){
    this._router.navigate(['/cashsettings'])
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }  
}