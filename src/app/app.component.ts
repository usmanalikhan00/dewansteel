import { Component } from '@angular/core';
import { Event, Router, NavigationEnd, NavigationStart } from  '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  showLoadingSpinner: any = false; 


  color = 'warn';
  mode = 'indeterminate';
  value = 50;
  bufferValue = 75;

  constructor(private _router: Router) {
		this._router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart){
        console.log("NAVIGATION START");
        this.showLoadingSpinner = true
      }
      if (routerEvent instanceof NavigationEnd){
        console.log("NAVIGATION END");
        this.showLoadingSpinner = false
      }
    });
  }
  // public userDB = new PouchDB('users');
}
