import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class PurchasePaymentService {
  
  authenticatedUser: any = [];
  public purchasePaymentsDB = new PouchDB('steelpurchasepayments');
  

  constructor(private _router: Router){}

  addPurchasePayment(purchasePayment){
    var self = this;
    console.log("PURCHASE PAYMENT DOCUMENT TO PUT:-----", purchasePayment)
    return self.purchasePaymentsDB.put(purchasePayment);
  }

  allPurchasePayments(){
    var self = this;
    return self.purchasePaymentsDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSinglePurchasePayment(purchaseId){
    var self = this;
    return self.purchasePaymentsDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.purchasePaymentsDB.find({
        selector: {_id:{$eq: purchaseId}}
      });
    })
  }

  // stockItemDetails(product){
  //   // PouchDB.debug.enable('pouchdb:find')
  //   var self = this;
  //   return self.invoicesReceiptsDB.createIndex({
  //     index: {
  //       fields: ['invoiceproducts']
  //     }
  //   }).then(function(){
  //     return self.invoicesReceiptsDB.find({
  //       selector: {
  //         invoiceproducts:{
  //           $elemMatch:{_id:{$eq:product._id}}
  //         }
  //       }
  //     });
  //   })
  // }

  getRefAccountPayments(accountrefid){
    var self = this;
    return self.purchasePaymentsDB.createIndex({
      index: {
        fields: ['accountrefid']
      }
    }).then(function(){
      return self.purchasePaymentsDB.find({
        selector: {accountrefid: {$eq: accountrefid}}
      });
    })
  }


}