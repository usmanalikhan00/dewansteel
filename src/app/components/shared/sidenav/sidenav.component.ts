import {Component, ViewChild, HostListener, ElementRef} from '@angular/core';
import {LoginService} from '../../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'side-nav',
    providers: [LoginService],
    templateUrl: 'sidenav.html',
    styleUrls: ['sidenav.css']
})

export class sidenav {

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  
  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }



  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }
  
  logout(){
    this._loginService.logout()
  }
  
}