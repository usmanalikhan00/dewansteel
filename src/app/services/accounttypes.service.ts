import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

// var users = [
//   new User('1','aajeel','admin@aajeel.com','adm9', 'admin'),
//   new User('2','khan','operator@aajeel.com','ope9', 'operator')
// ];

@Injectable()
export class AccountTypesService {
  
  // authenticatedUser: any = [];
  public accountTypesDB = new PouchDB('steelaccounttypes', {auto_compaction: true});
  public subAccountsDB = new PouchDB('steelsubaccounts', {auto_compaction: true});
  

  constructor(private _router: Router){}

  addAccountType(values){
    var self = this;
    return self.accountTypesDB.put(values)
  }
  
  getAccountTypes(){
    var self = this;
    return self.accountTypesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }  

  
  
  updateAccountBalance(account){
    var self = this
    return self.accountTypesDB.bulkDocs([account]) 
  }

  updateBulkAccounts(accounts){
    var self = this
    return self.accountTypesDB.bulkDocs(accounts) 
  }



  addSubAccount(values){
    var self = this;  
    console.log("SUB ACCOUNT IN SERVICE:-----", values)
    // return self.subAccountsDB.put({
    //   _id: new Date().toISOString(),
    //   name: name,
    //   accountId: accountId
    // })
    return self.subAccountsDB.put(values)
  }

  getSingleAccount(accountId){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {_id:{$eq: accountId}}
      });
    })
  }



  getSingleAccountItems(accountId){
    var self = this;
    return self.subAccountsDB.createIndex({
      index: {
        fields: ['accountId']
      }
    }).then(function(){
      return self.subAccountsDB.find({
        selector: {accountId: {$eq: accountId}}
      });
    })
  }

  getAccountSubAccounts(accounttype){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['accounttype']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {accounttype: {$eq: accounttype}}
      });
    })
  }

  getParentLevelAccounts(){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['level']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {level: {$eq: 'parent'}}
      });
    })
  }


  getAllBankAccounts(){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['accounttype']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {accounttype: {$eq: 'bank'}}
      });
    })
  }


  getChildLevelAccounts(account){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['level']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {
          $and:[
            {parentid: {$eq: account._id}},
            {level: {$eq: 'child'}}
          ]
        }
      });
    })
  }




  getCustomerAccounts(customerid){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['customerid']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {
          $and:[
            {customerid: {$eq: customerid}},
            {expirystatus: {$eq: 'pending'}},
            {obtype: {$eq: 'Debit'}}
          ]
        }
      });
    })
  }

  getPendingAccounts(){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['expirystatus']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {
          expirystatus: {$eq: 'pending'},
        },
        limit: 5
      });
    })
  }



  getCustomerCreditAccounts(customerid){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['customerid']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {
          $and:[
            {customerid: {$eq: customerid}},
            {expirystatus: {$eq: 'pending'}},
            {obtype: {$eq: 'Credit'}}
          ]
        }
      });
    })
  }



}