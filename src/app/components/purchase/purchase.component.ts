import { Component, ElementRef, ViewChild, HostListener, Inject } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import {ElectronService} from 'ngx-electron'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { MatSidenav, MatTableDataSource, MatPaginator, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA  } from "@angular/material";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { CustomerDebitCreditService } from '../../services/customerdebitcredit.service'
import { ItemDebitCreditService } from '../../services/itemdebitcredit.service'
import { CustomersService } from '../../services/customers.service'
import * as _ from 'lodash'

export interface UserData {
  purchasenumber ? : string;
  createdby ? : string;
  purchasecustomer ? : string;
  productcount ? : number;
  purchasenotes ? : string;
  purchasedate ? : string;
  challanno ? : string;
  billno ? : string;
  purchasetotal ? : number;
  othercharges ? : number;
  createdat ? : string;
  _id ? : string;
  _rev ? : string;
  found ? : boolean;
}

@Component({
    selector: 'purchase',
    providers: [LoginService, 
                ProductService, 
                InvoiceService, 
                PurchaseService, 
                ElectronService, 
                CustomerDebitCreditService,
                ItemDebitCreditService,
                CustomersService],
    templateUrl: 'purchase.html',
    styleUrls: ['purchase.css']
})

export class purchase {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  public purchaseDB = new PouchDB('steelcustomerpurchase');
  public localPurchasesDB = new PouchDB('http://localhost:5984/localsteelpurchases');
  // public ibmPurchasesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewanpurchases', {
  //   auth: {
  //     "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
  //     "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
  //     "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
  //     "port": 443,
  //     "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
  //   }
  // });
  public ibmPurchasesDB = new PouchDB('https://dewansteel.cloudant.com/dewanpurchases', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });
  
  public cloudantPurchasesDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewanpurchases', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });

  dateFilterForm: FormGroup;
  
  allPurchases: any = [];
  purchasesTotal: any = null;

  displayedColumns = [
    "createdat",
    "challanno",
    "billno",
    'purchasenumber', 
    'createdby', 
    'purchasecustomer', 
    'productcount', 
    "purchasenotes",
    "purchasetotal",
    "action"
  ];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);

  toDate: any = null
  fromDate: any = null
  authUser: any = null
  allPurchasesDC: any = []
  allCustomers: any = []
  // allPurchases: any = []
  animal: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _customerDebitCreditService: CustomerDebitCreditService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _customersService: CustomersService, 
              private _formBuilder: FormBuilder,
              public _dialog: MatDialog, 
              private _electronService: ElectronService, 
              private _router: Router) {
    this.loadDataSource()
    this._buildAddProductForm()
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }

  private _buildAddProductForm(){
    this.dateFilterForm = this._formBuilder.group({
      fromdate: [''],
      todate: ['']
    })
  }

  ngOnInit(){
    var self = this
    self.getAllCustomers()
    // self.syncDb()
  }

  syncDb(){
    var self = this
    // var sync = PouchDB.sync(self.purchaseDB, self.ibmPurchasesDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (info) {
    //   console.log("CHANGE EVENT", info)
    //   if (info.direction === 'pull'){
    //     self.loadDataSource()
    //   }
    //   // handle change
    // }).on('paused', function (err) {
    //   // replication paused (e.g. replication up to date, user went offline)
    //   console.log("PAUSE EVENT FROM PURCHASE", err)
    // }).on('active', function () {
    //   // replicate resumed (e.g. new changes replicating, user went back online)
    //   console.log("ACTIVE ACTIVE !!")
    // }).on('denied', function (err) {
    //   // a document failed to replicate (e.g. due to permissions)
    //   console.log("DENIED DENIED !!", err)
    // }).on('complete', function (info) {
    //   // handle complete
    //   console.log("COMPLETED !!", info)
    // }).on('error', function (err) {
    //   // handle error
    //   console.log("ERROR ERROR !!", err)
    // });
    var opts = { live: true, retry: true };
    self.purchaseDB.replicate.from(self.cloudantPurchasesDB).on('complete', function(info) {
      // console.log("COMPLETE EVENT FROM ONE-WAY PURCHASES REPLICATION:--", info)
      self.purchaseDB.sync(self.cloudantPurchasesDB, opts).on('change', function (info) {
        // console.log("CHANGE EVENT FROM TWO-WAY PURCHASES SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
          // self.loadDataSource()
        // }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC PURCHASES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY PURCHASES SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR !!", err)
    })
  }

  goToPrint(){
    // this._router.navigate(['printpurchases'])
    this._electronService.ipcRenderer.send('PurchasesPrint')
  }

  loadDataSource(){
    var self = this;
    var purchaseDC = 0
    var noPurchaseDC = 0
    self._purchaseService.allPurchases().then(function(result){
      const users: UserData[] = [];
      self.allPurchases = [];
      result.rows.map(function(row){
        // users.push(createNewUser(row.doc))
        self.allPurchases.push(row.doc); 
      })
      self._customerDebitCreditService.getPurchaseDebitCredits().then(function(result){
        self.allPurchasesDC = result.docs
        for (let obj of self.allPurchases){
          var dataKey = _.filter(self.allPurchasesDC, {dcrefid:obj._id})
          if (dataKey[0]){
            purchaseDC++
            obj.found = true
          }
          else{
            noPurchaseDC++
            obj.found = false
          }
          users.push(createNewUser(obj))
        }
        // console.log("ALL PURCHASES:-----", self.allPurchases)
        // console.log("ALL DEBIT/CREDIT FOR PUCRHASE:-----", self.allPurchasesDC)
        // console.log("UPDATED PURCHASE COUNT:-----", purchaseDC, noPurchaseDC)
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getPurchasesTotal(self.allPurchases)
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err);
    })

  }

  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getCustomerPurchaseDC(){
    var self = this
    var purchaseDC = 0
    self._customerDebitCreditService.getPurchaseDebitCredits().then(function(result){
      self.allPurchasesDC = result.docs
      for (let obj of self.allPurchasesDC){
        var dateKey = _.filter(self.allPurchases, {_id:obj.dcrefid})
        // console.log(dateKey[0])
        obj.dcrefdate = dateKey[0].purchasedate
        if (dateKey[0])
          purchaseDC++
      }
      // console.log("ALL DEBIT/CREDIT FOR PUCRHASE:-----", self.allPurchasesDC)
      // console.log("UPDATED PURCHASE COUNT:-----", purchaseDC)
      // self.updateInvoiceDC(self.allPurchasesDC)
    }).catch(function(err){
      console.log(err)
    })


  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log("EVENT ROM DATE CHANGE:----", event.value)
    // this.events.push(`${type}: ${event.value}`);
    if (type === 'input' || type === 'change'){
      this.fromDate = event.value.toISOString()
    }
    if (type === 'input2' || type === 'change2'){
      this.toDate = event.value.toISOString()
    }
    // console.log("TO DATE:----\n", this.toDate, "\n", "FROM DATE:----\n", this.fromDate)

    if (this.fromDate < this.toDate){
      // console.log("FROM DATE  SMALL")
    }else{
      // console.log("FROM DATE LARGER")
    }
    if (this.toDate < this.fromDate){
      // console.log("TO DATE  SMALL")
    }else{
      // console.log("TO DATE LARGER")
    }

    if (this.toDate != null && this.fromDate != null){

      var self = this
      self._purchaseService.filterPurchases(self.toDate, self.fromDate).then(function(result){
        // console.log("RESULT FROM FILTER DEBIT CREDIT:--", result)
        // self.allInvoices = []
        // result.docs.forEach(function(doc){
        //   self.allInvoices.push(doc)
        // })
        const users: UserData[] = [];
        self.allPurchases = [];
        result.docs.forEach(function(row){
          users.push(createNewUser(row))
          self.allPurchases.push(row)
        })
        self.dataSource = new MatTableDataSource(users);
        self.dataSource.paginator = self.paginator;
        self.dataSource.sort = self.sort;
        self.getPurchasesTotal(self.allPurchases)
      }).catch(function(err){
        console.log(err)
      })
    }

  }


  clearFilters(){
    (<FormGroup>this.dateFilterForm).setValue({'fromdate':'', 'todate':''}, {onlySelf: true})
    this.loadDataSource()
    this.toDate = null
    this.fromDate = null
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  getPurchasesTotal(allPurchases){
    var sum = 0
    for (let doc of allPurchases){
      sum = sum + doc.purchasetotal
    }
    this.purchasesTotal = sum
    // this.invoicesTotal = Number(this.invoicesTotal)
    // console.log("SUM FROM PURCHASES TOTAL:===", sum, this.purchasesTotal)
  }

  getSinglePurchase(purchase){
    // console.log("Selected Purchase:===", purchase)
    this._router.navigate(['/purchase', purchase._id])
  }

  openLedgerDialog(item): void {
    var self = this
    var selectedPurchase = null
    // console.log("ITEM CLICKED:------", item)
    self._purchaseService.getSinglePurchase(item._id).then(function(result){
      // console.log("RESULT FROM SINGLE PURCHASE:--", result);
      selectedPurchase = result.docs[0]
      var customerToUpdate = _.filter(self.allCustomers, {"_id": result.docs[0].purchasecustomer._id})
      if (!item.found){
        // console.log("ENTRY NOT FOUND");
        customerToUpdate[0].closingbalance = customerToUpdate[0].closingbalance - result.docs[0].purchasetotal
        var customerDebitCredit = {
          "_id": new Date().toISOString(),
          "name": result.docs[0].purchasecustomer.name,
          "address": result.docs[0].purchasecustomer.address,
          "phone": result.docs[0].purchasecustomer.phone,
          "fax": result.docs[0].purchasecustomer.fax,
          "email": result.docs[0].purchasecustomer.email,
          "customerid": customerToUpdate[0]._id,
          "customerrev": customerToUpdate[0]._rev,
          "openingbalance": customerToUpdate[0].openingbalance,
          "closingbalance": customerToUpdate[0].closingbalance,
          "dcref": selectedPurchase.purchasenumber,
          "dcrefid": selectedPurchase._id,
          "dcrefdate": selectedPurchase.purchasedate ? selectedPurchase.purchasedate : new Date().toISOString(),
          "dctype": "purchase",
          "challanno": selectedPurchase.challanno ? selectedPurchase.challanno : '',
          "notes": selectedPurchase.purchasenotes,
          "billno": selectedPurchase.billno ? selectedPurchase.billno : '',
          "status": null,
          "debit": null,
          "credit": null
        }
        if (customerToUpdate[0].closingbalance >= 0){
          customerToUpdate[0].status = 'debit'
          customerDebitCredit.status = 'debit'
          customerDebitCredit.credit = -selectedPurchase.purchasetotal
          customerDebitCredit.closingbalance = customerToUpdate[0].closingbalance
        }else if (customerToUpdate[0].closingbalance <= 0){
          customerToUpdate[0].status = 'credit'
          customerDebitCredit.status = 'credit'
          customerDebitCredit.credit = -selectedPurchase.purchasetotal
          customerDebitCredit.closingbalance = customerToUpdate[0].closingbalance
        }
        // console.log("CUSTOMER DEBIT CREDIT:--", customerDebitCredit);
        // console.log("\nCUSTOMER TO UPDATE:--", customerToUpdate[0]);
        // self.getAllCustomers()
        self._customerDebitCreditService.addDebitCredit(customerDebitCredit).then(function(result){
          // console.log("RESULTA AFTER ADDING DEBIT CREDIT:--", result)
          self._customersService.updateCustomerBalance(customerToUpdate[0]).then(function(result){
            // console.log("RESULT UPDATING ACTUAL CUSTOMER:--", result)
            let dialogRef = self._dialog.open(purchaseDetailDialog, {
              width: '100%',
              height: '600px',
              data: { info: selectedPurchase, dcref: 'purchase' }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('The Ledger detail was closed', result);
              self.getAllCustomers()
              self.loadDataSource()
            })
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }else{
        // console.log("ENTRY FOUND");
        let dialogRef = self._dialog.open(purchaseDetailDialog, {
          width: '100%',
          height: '600px',
          data: { info: selectedPurchase, dcref: 'purchase' }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('The Ledger detail was closed', result);
        })
      }
    }).catch(function(err){

      console.log(err);
    })


  }


  openDeleteDialog(purchase): void {
    // var self = this;
    // console.log("PRODUCT CLICKED:------", product)
    let dialogRef = this._dialog.open(pucrhaseDeleteDialog, {
      width: '250px',
      data: { name: purchase.purchasenumber }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
      console.log('The dialog was closed', result, this.animal);
      if (this.animal){
        // alert(1)
        this.getPurchaseDc(purchase)
        // this.getProductPurchaseDc(purchase)
        // console.log('YES TYPED:--', purchase);
        this.removeProduct(purchase)
      }
    })
  }

  removeProduct(purchase){
    var self = this
    purchase._deleted = true
    self._purchaseService.addPurchase(purchase).then(function(result){
      console.log("RESULT AFTER DELETING PURCHASE:--", result)
      self.loadDataSource()
    }).catch(function(err){
      console.log(err)
    })
  }

  getPurchaseDc(purchase){
    var self = this;
    var customerDC: any = null
    var productsDC: any = []
    self._customerDebitCreditService.getSinglePurchaseDebitCredit(purchase).then(function(result){
      result.docs.forEach(function(row){
        row._deleted = true
        customerDC = row
      })
      console.log("RESULT FROM SINGLE ENTRY OF CUSTOMER DC:--", customerDC)
      self._itemDebitCreditService.getSinglePurchaseDebitCredit(purchase).then(function(result){
        result.docs.forEach(function(row){
          row._deleted = true
          productsDC.push(row)
        })
        console.log("RESULT FROM SINGLE ENTRY OF PRODUCT DC:--", productsDC)
        self._itemDebitCreditService.addDebitCredit(productsDC).then(function(result){
          console.log("RESULT AFTER DELETING PRODUCT DC:--", result)
          self._customerDebitCreditService.addDebitCredit(customerDC).then(function(result){
            console.log("RESULT AFTER DELETING CUSTOMER DC:--", result)
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })

  }

  getProductPurchaseDc(purchase){
    var self = this;
  }




  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }  

  logout(){
    this._loginService.logout()
  }  
}


function createNewUser(row): UserData {
  // console.log("PURCHASE ROW TO SHOW:----", row)
  return {
    "_id": row._id,
    "_rev": row._rev,
    "purchasecustomer" : row.purchasecustomer.name.toString(),
    "createdat" : row.createdat.toString(),
    "createdby" : row.createdby._id.toString(),
    "purchasenotes" : row.purchasenotes,
    "purchasenumber" : row.purchasenumber.toString(),
    "purchasetotal" : row.purchasetotal,
    "billno" : row.billno,
    "challanno" : row.challanno,
    "purchasedate" : row.purchasedate,
    "found" : row.found,
    "productcount" : row.purchaseproducts.length
  }

}

@Component({
  selector: 'purchase-detail',
  templateUrl: 'purchase-detail.html',
  providers:[ElectronService],
  styleUrls: ['purchase.css']
})
export class purchaseDetailDialog {

  constructor(
    public dialogRef: MatDialogRef<purchaseDetailDialog>,
    public _electronService: ElectronService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(){
    // console.log("POP UP START DATA:---", this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(data){
    console.log(data)
    this._electronService.ipcRenderer.send('PurchasePrint', data._id);
  }

}


@Component({
  selector: 'purcchase-delete-dialog',
  templateUrl: 'purcchase-delete-dialog.html'
})
export class pucrhaseDeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<pucrhaseDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void{

  }

}