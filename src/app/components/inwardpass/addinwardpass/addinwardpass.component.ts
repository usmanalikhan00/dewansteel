import { Component, ElementRef, ViewChild, HostListener, Output, Input, EventEmitter } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { StoreSettingsService } from '../../../services/storesettings.service'
import { CustomersService } from '../../../services/customers.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { InwardPassService } from '../../../services/inwardpass.service'
import { ItemDebitCreditService } from '../../../services/itemdebitcredit.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { MatSidenav, MatSnackBar } from "@angular/material";

@Component({
    selector: 'add-inwardpass',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                InwardPassService, 
                ItemDebitCreditService, 
                CustomersService],
    templateUrl: 'addinwardpass.html',
    styleUrls: ['addinwardpass.css']
})

export class addinwardpass {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  // public productsDB = new PouchDB('steelinvoice');
  public purchaseDB = new PouchDB('steelcustomerinwardpass');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  invoiceProducts: any = [];
  allInvoices: any= [];
  
  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;
  addProductForm: FormGroup;
  
  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  cartProducts: any[] = []
  cartTotal: any = 0
  cartWeight: any = 0
  
  cartStates: Observable<any[]>;
  inwardpassNotes: any = null;

  newProductFlag: boolean = false
  newProductStores: any = []

  productTypes = [
    'Weighted',
    'Single Unit'
  ];
  // formstores =  new FormControl()

  allCategories: any = [];
  allSubCategories: any = [];
  selectedCategory: any = null;
  authUser: any = null;
  selectedStores: any = [];
  stockFlag: boolean = false;

  autoFiller: any = 1
  inwardpassdate: any = null
  voucherno: any = null

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _matSnackBar: MatSnackBar, 
              private _inwardPassService: InwardPassService, 
              private _itemDebitCreditService: ItemDebitCreditService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    this._buildAddProductForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }

  private _buildAddProductForm(){
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      formstores:['', Validators.required]
    })
  }



  ngOnInit(){
    this.getCartProducts()
    this.getAllStores()
    this.getAllCustomers()
    this.getAllCategories()
    // this.allSubCategories()
  }
  
  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      // console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }


  selectSubCategory($event){
    var self = this;
    // console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }


  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      self.customerStates = self.addCustomerForm.controls['customerCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.allCustomers.slice());
      // console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getPurchaseNotes($event){
    this.inwardpassNotes = $event.target.value
    // console.log("INWARD PASS NOTES ARE:-----", $event ,this.inwardpassNotes)
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self.newProductStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
        self.newProductStores.push(row.doc)
      });
      self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStores(state) : self.allStores.slice());
      // console.log("ALL Stores:=====", self.allStores);
      // console.log("ALL NEW PRODUCT Stores:=====", self.newProductStores);
    }).catch(function(err){

    })
  }
  

  sortCart(cartProducts){
    var sortByProperty = function (property) {
      return function (x, y) {
          return ((x[property] === y[property]) ? 0 : ((x[property] < y[property]) ? 1 : -1));
      }
    }
    cartProducts.sort(sortByProperty('sortorder'))
  }

  getCartProducts(){
    var self = this;
    self.invoiceProducts = [];
    self.states = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc);
        var object = {
          "name":row.doc.name,
          "weight":row.doc.weight,
          "batchnumber":row.doc.batchnumber,
          "stockvalue":row.doc.stockvalue,
          "storeStocks":row.doc.storeStocks,
          "_id":row.doc._id,
          "_rev":row.doc._rev,
          "category":row.doc.category,
          "categoryname":row.doc.categoryname,
          "subcategory":row.doc.subcategory,
          "price":row.doc.price,
          "cartweight":0,
          "calcweight":0,
          "type":row.doc.type,
          "stores":row.doc.stores,
          "cartstores":"",
          "cartquantity":0,
          "passquantity":0,
          "cartprice":0,
          "module":"inwardpass"
        }   
        self.states.push(object)
      });
      // console.log("ALL PRODUCTS:=====", self.invoiceProducts);
      // self.states = [];
      // for (let item of self.invoiceProducts){
        
      //     var object = {
      //       "name":item.name,
      //       "weight":item.weight,
      //       "batchnumber":item.batchnumber,
      //       "stockvalue":item.stockvalue,
      //       "storeStocks":item.storeStocks,
      //       "_id":item._id,
      //       "_rev":item._rev,
      //       "category":item.category,
      //       "categoryname":item.categoryname,
      //       "subcategory":item.subcategory,
      //       "price":item.price,
      //       "cartweight":0,
      //       "calcweight":0,
      //       "type":item.type,
      //       "stores":item.stores,
      //       "cartstores":"",
      //       "cartquantity":0,
      //       "passquantity":0,
      //       "cartprice":0,
      //       "module":"inwardpass"
      //     }          

      //   self.states.push(object)
      // }
      // var newProduct = {
      //   "type":"new_product",
      //   "name":"New Product",
      //   "price":"",
      //   "stockvalue":""

      // }
      // self.states.push(newProduct)
      self.filteredStates = self.addInvoiceForm.controls['stateCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStates(state) : self.states.slice());
    }).catch(function(err){
      console.log(err);
    })
  }

  addInwardpass(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    var ID = Math.floor(Math.random()*1000) + 100000
    var inwardpassDoc: any = null
    self.selectedCustomer.balance = self.cartTotal
    if (self.inwardpassdate === null){
      inwardpassDoc = {
        "_id": new Date().toISOString(),
        "inwardpassnumber": ID, 
        "inwardpasstotal": self.cartTotal, 
        "inwardpassweight": self.cartWeight, 
        "inwardpassproducts": self.cartProducts, 
        "inwardpasscustomer": self.selectedCustomer, 
        "status": null, 
        "voucherno": self.voucherno, 
        "inwardpassdate": self.inwardpassdate, 
        "createdby": user, 
        "createdat": moment().format(),
        "inwardpassnotes":self.inwardpassNotes,
        "converted":false 
      }
    }else{
      inwardpassDoc = {
        "_id": new Date().toISOString(),
        "inwardpassnumber": ID, 
        "inwardpasstotal": self.cartTotal, 
        "inwardpassweight": self.cartWeight, 
        "inwardpassproducts": self.cartProducts, 
        "inwardpasscustomer": self.selectedCustomer, 
        "status": null, 
        "voucherno": self.voucherno, 
        "inwardpassdate": self.inwardpassdate.toISOString(), 
        "createdby": user, 
        "createdat": moment().format(),
        "inwardpassnotes":self.inwardpassNotes,
        "converted":false 
      }
    }
    console.log("PURCHASE DOC TO PUSH:=====", inwardpassDoc);
    var storeFlag = false
    for (let doc of inwardpassDoc.inwardpassproducts){
      if (doc.cartstores === ""){
        storeFlag = true
        break
      }else{
        storeFlag = false
      }
    }
    // console.log("STORES NOT SELECTED:=====", storeFlag);
    if (!storeFlag){
      self._inwardPassService.addInwardpass(inwardpassDoc).then(function(result){
        // console.log("RESULT AFTER ADDING INWARD PASS:=======", result)
        var productDocs: any = []
        var purchaseDocs: any = []
        var itemDebitCredit: any = []
        for (let product of self.invoiceProducts){
          for (let doc of self.cartProducts){
            if (product._id === doc._id){
              var sum = 0
              for (let obj of doc.stores){
                if (obj._id === doc.cartstores._id){
                  obj.stock = obj.stock + doc.cartquantity
                }  
                sum = sum + obj.stock
              }
              doc.totatcartweight = doc.cartquantity * doc.weight
              doc.stockvalue = sum
              if (product.type === "Weighted"){
                doc.netweight = doc.stockvalue * doc.weight
                doc.networth = doc.stockvalue * doc.weight * doc.price
              }
              if (product.type === "Single Unit"){
                doc.networth = doc.stockvalue * product.price
              }
              var object = {
                _id:(Math.floor(Math.random()*1000) + 100000).toString(),
                name:doc.name,
                productid:doc._id,
                productrev:doc._rev,
                debit:null,
                credit:doc.cartquantity,
                // createdat:result.id,
                voucherno:self.voucherno,
                inwardpassdate:self.inwardpassdate,
                customer:self.selectedCustomer,
                dcref: inwardpassDoc.inwardpassnumber,
                dctype: "inwardpass",
                stockvalue:doc.stockvalue
              }
              itemDebitCredit.push(object)              
              purchaseDocs.push(doc)
              productDocs.push(product)
            }
          }
        }
        // console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
        // console.log("PRODUCT DOCS IN INWARD PASS:=====", purchaseDocs);
        // console.log("ITEM DEIT CREDIT OBJECT ARRAY:=====", itemDebitCredit);
        // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
        self._productService.updateInvoiceProducts(purchaseDocs).then(function(result){
          // console.log("RESULT AFTER UPDATEING INWARD PRODUCTS:--------", result)
          // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
          self._itemDebitCreditService.addDebitCredit(itemDebitCredit).then(function(result){
            // console.log("RESULT AFTER ITEM CREDIT DEBIT ADDITION:--------", result)
            self.cartProducts = []
            self.selectedCustomer = null
            self.newProductFlag = false
            self.inwardpassNotes = null
            self.autoFiller = 1
            self.inwardpassdate = null
            self.voucherno = null
            self.getCartProducts()
          }).catch(function(err){
            console.log(err)
          })
        }).catch(function(err){
          console.log(err)
        })
      }).catch(function(err){
        console.log(err)
      })
    }else{
      this._matSnackBar.open('Must select a store', 'Undo', {
        duration: 3500
      });
    }

  }


  newProductAdded($event){
    // console.log("EVENT FROM ADD PRODUCT FORM:-----", $event)
    this.getCartProducts()
  }

  setProductStore($event, product){
    // this.selectedStore = $event.value
    console.log("Selected Store:----", $event.value)
    for (let store of product.stores){
      if (store._id === $event.value){
        product.cartstores = store
        // this.selectedStore = store
      }
    }
    // product.cartstores = this.selectedStore
  }

  selectCustomer(state){

    this.selectedCustomer = state
    // console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)
    this._buildAddCustomerForm()
    this.getAllCustomers()
  }



  addNewProduct(values){

    var self = this;
    console.log("VALUES FROM ADD PRODUCT FORM:", values)
    
    self.selectedStores = []
    for (let store of values.formstores){
      store.stock = 0
    }
    self.selectedStores = values.formstores

    var ID = Math.floor(Math.random()*100000000) + 800000000
    console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    // console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    // console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
    // console.log("MULTI SELECT STORE VALUES:--", this.toppings.value);

    self._productService.addProduct(values).then(function(result){
      // console.log("RESULT AFTER ADDING THE PRODUCT:-------", result)
      self.addProductForm.reset()
      self.getCartProducts()
      self.newProductFlag = false
    }).catch(function(err){
      console.log(err)
    })
  }


  selectCartProduct(item){
    if (item.type === 'new_product'){
      this.newProductFlag = true
      this._buildAddInvoiceForm()
      this.getCartProducts()
    }else{
      // console.log("AUTO SELCT CHANGE FUNCTION", item)
      this.selecteProduct(item)
      // this._buildAddInvoiceForm()
    }

  }


  selecteProduct(product){
    // console.log("CHANGE INPUT VALUE SELECED:_+_", product)
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.cartquantity = product.cartquantity + 1
        product.passquantity = product.passquantity + 1
        if (product.type == 'Weighted'){
          // product.calcweight = product.cartquantity * product.cartweight
          product.cartweight = product.calcweight/product.cartquantity
          product.cartworth = product.cartquantity * product.cartweight * product.cartprice
        }else{
          product.calcweight = null
          product.cartworth = product.cartquantity * product.cartprice
        }
        product.sortorder = this.autoFiller
        this.autoFiller++        
        this.cartProducts.push(product)
        // this.cartProducts = _.reverse(this.cartProducts)
        this.sortCart(this.cartProducts)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].cartquantity = this.cartProducts[i].cartquantity + 1
            this.cartProducts[i].passquantity = this.cartProducts[i].passquantity + 1
            if (this.cartProducts[i].type == 'Weighted'){
              // this.cartProducts[i].calcweight = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight
              this.cartProducts[i].cartweight = this.cartProducts[i].calcweight/this.cartProducts[i].cartquantity
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight * this.cartProducts[i].cartprice
            }else{
              this.cartProducts[i].calcweight = null
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            // console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.cartquantity = product.cartquantity + 1
          product.passquantity = product.passquantity + 1
          if (product.type == 'Weighted'){
            // product.calcweight = product.cartquantity * product.cartweight
            product.cartweight = product.calcweight/product.cartquantity
            product.cartworth = product.cartquantity * product.cartweight * product.cartprice
          }else{
            product.calcweight = null
            product.cartworth = product.cartquantity * product.cartprice
          }
          product.sortorder = this.autoFiller
          this.autoFiller++
          this.cartProducts.push(product)
          // this.cartProducts = _.reverse(this.cartProducts)
          this.sortCart(this.cartProducts)
          this.calculateTotal(this.cartProducts)
        }
      }

      this._buildAddInvoiceForm()
      // this.getCartProducts()
      this.filteredStates = this.addInvoiceForm.controls['stateCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? this.filterStates(state) : this.states.slice());
    }
    
  }

  changeCalcWeight(calcweight, product){
    // console.log("CALC WEIGHT CAHFGES:_+_", calcweight, product)
    product.calcweight = calcweight
    product.cartweight = product.calcweight/product.cartquantity
    this.calculateWeight(this.cartProducts)
  }

  changeWeight(weight, product){
    product.cartweight = weight
    product.calcweight = product.cartquantity * product.cartweight
    product.cartworth = product.calcweight * product.cartprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    // console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    if (weight <= 0){
      product.cartweight = product.weight
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.calcweight * product.cartprice
      this._matSnackBar.open('Weight must be > 0', 'Undo', {
        duration: 3500
      });      
    }
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    // console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.cartquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight
        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      product.passquantity = product.cartquantity
      this.calculateTotal(this.cartProducts)
      // this.calculateWeight(this.cartProducts)
    }
    if (quantity <= 0){
      product.cartquantity = 1
      product.passquantity = product.cartquantity
      if (product.type == 'Weighted'){
        // product.calcweight = product.cartquantity * product.cartweight
        product.cartweight = product.calcweight/product.cartquantity
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      this._matSnackBar.open('Quantity must be > 0', 'Undo', {
        duration: 3500
      });
    }
  }

  changePrice(price, product){
    product.cartprice = price
    if (product.type == 'Weighted'){
      // product.calcweight = product.cartquantity * product.cartweight
      product.cartweight = product.calcweight/product.cartquantity
      product.cartworth = product.cartquantity * product.cartweight * product.cartprice
    }else{
      product.calcweight = null
      product.cartworth = product.cartquantity * product.cartprice
    }

    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    this.calculateTotal(this.cartProducts)
  }




  calculateTotal(carItems){
    var total = 0  
    for (let item of carItems){
      total = total + item.cartworth
    }
    this.cartTotal = total
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  calculateWeight(carItems){
    var total = 0  
    for (let item of carItems){
      total = total + item.calcweight
    }
    this.cartWeight = total
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }



  deleteCartItem(i, cartProducts){
    // console.log("DELETE CART ITEM CALLED:-", i, cartProducts)
    cartProducts.splice(i, 1)
    this.cartProducts = cartProducts
    this.calculateTotal(this.cartProducts)

  }


  filterStates(name: string) {
    // console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers(name: string) {
    // console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.allCustomers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    // console.log("ALL Customers:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }

}
