import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'sub-categories',
    providers: [LoginService, ProductService],
    templateUrl: 'subcategories.html',
    styleUrls: ['subcategories.css']
})

export class subcategories {
  
  public productSubCategoriesDB = new PouchDB('productsubcategories');

  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  addSubCategoryForm: FormGroup;
  allSubCategories: any = []
  sub: any;
  categoryId: any;
  selectedCategory: any;
  authUser: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddSubCategoryForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }


  private _buildAddSubCategoryForm(){
    this.addSubCategoryForm = this._formBuilder.group({
      name: ['', Validators.required]
    })
  }
  // ngOnInit() {
  //   this.sub = this._activatedRouter.params.subscribe(params => {
  //      this.categoryId = params['categoryId']; // (+) converts string 'id' to a number
  //   });
  // }
      // this._route.paramMap
      // .switchMap((params: ParamMap) =>
      //   this._demandService.updateDemandStatus(this.demands[i]._id,demState))
      // .subscribe(demands =>{
      //   console.log("Dem Status Resp",demands)
      // });

  ngOnInit(){
    var self = this;
    // this._activatedRouter.paramMap
    //   .switchMap((params: ParamMap) => )
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.categoryId = params['categoryId']; // (+) converts string 'id' to a number
      console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.categoryId)
      // self._productService.getSingleCategory(this.categoryId).then(function (doc) {
      //   // handle doc
      // }).catch(function (err) {
      //   console.log(err);
      // })
      self.getAllSubCategories()

    });
  }
  
  getAllSubCategories(){
    var self = this;
    self._productService.getSingleCategory(self.categoryId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedCategory = result.docs[0]
      console.log("RESULT FROM SINLE CATEGORY", self.selectedCategory)
      self._productService.getSingleCategoryItems(self.categoryId).then(function(doc){
        console.log(doc)
        self.allSubCategories = [];
        // self.selectedCategory = doc;
        for (let row of doc.docs){
          self.allSubCategories.push(row)
        }
        console.log("ALL SUB CATEGORIES OF THIS CATEGORY:----", self.allSubCategories)
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
        console.log(err)
    })
    // self._productService.allProductSubCategories().then(function(result){
    //   result.rows.map(function (row) { 
    //     self.allSubCategories.push(row.doc); 
    //   });
    //   console.log("ALL PRODUCT SUB CATEGORIES:=====", self.allSubCategories);
    // }).catch(function(err){
    //   console.log(err);
    // })
  }

  addSubCategory(values){
    var self = this;
    console.log("PRODUCT SUB CATEGORY to add:=====", values);
    self._productService.addProductSubCategory(values.name, self.categoryId).then(function(result){
      console.log("CATEGORY ADDED:---------", result)
      self._buildAddSubCategoryForm()
      self.getAllSubCategories()
    }).catch(function(err) {
      console.log(err)
    })
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  goBack(){
    this._router.navigate(['productcategories'])
  }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
