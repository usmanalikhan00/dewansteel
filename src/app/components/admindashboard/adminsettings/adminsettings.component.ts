import {Component, ElementRef, ViewChild, HostListener} from '@angular/core';
import {LoginService} from '../../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'admin-settings',
    providers: [LoginService],
    templateUrl: 'adminsettings.html',
    styleUrls: ['adminsettings.css']
})

export class adminsettings {
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';

  authUser: any = null

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))

  }

  private _buildAddProductForm(){
    
  }

  ngOnInit(){
    if (window.innerWidth < 886) {
      this.navMode = 'over';
    }
  }



  goBack(){
    if (this.authUser._id === 'admin')
      this._router.navigate(['admindashboard'])
    else if (this.authUser._id === 'deo')
      this._router.navigate(['deodashboard'])
  }  
  
  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  
}