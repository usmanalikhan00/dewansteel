import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class PurchaseService {
  
  authenticatedUser: any = [];
  public purchaseDB = new PouchDB('steelcustomerpurchase');
  

  constructor(private _router: Router){}

  addPurchase(purchaseDoc){
    console.log("PURCHASE DOCUMENT TO PUT:-----", purchaseDoc)
    var self = this;
    return self.purchaseDB.put(purchaseDoc);
  }

  allPurchases(){
    var self = this;
    return self.purchaseDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSinglePurchase(purchaseId){
    var self = this;
    return self.purchaseDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.purchaseDB.find({
        selector: {_id:{$eq: purchaseId}}
      });
    })
  }


  stockItemDetails(product){
    // PouchDB.debug.enable('pouchdb:find')
    var self = this;
    return self.purchaseDB.createIndex({
      index: {
        fields: ['purchaseproducts']
      }
    }).then(function(){
      return self.purchaseDB.find({
        selector: {
          purchaseproducts:{
            $elemMatch:{_id:{$eq:product._id}}
          }
        }
      });
    })
  }

  customerPurchases(customer){
    // PouchDB.debug.enable('pouchdb:find')
    console.log("CUSTOMER WHOSE INVOOICES WE WANT TO GET IN SERVCIEL_-----", customer)
    var self = this;
    return self.purchaseDB.createIndex({
      index: {
        fields: ['invoicecustomerid']
      }
    }).then(function(){
      return self.purchaseDB.find({
        selector: {
          purchasecustomerid:{$eq:customer._id}
        }
      });
    })
  }

  updateReturnFlag(purchase){
    var self = this
    return self.purchaseDB.bulkDocs(purchase)
  }

  filterPurchases(toDate, fromDate){
    var self = this;
    console.log(fromDate, toDate)
    return self.purchaseDB.createIndex({
      index: {
        fields: ['_id', 'createdat']
      }
    }).then(function(){
      return self.purchaseDB.find({
        selector: { 
          $and: [
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $lt: toDate
              }
            },
            {
              _id:{
                $gte: fromDate
              }
            }

          ]
        },
        sort: [{_id:'asc'}]
      })
    })
  }


}