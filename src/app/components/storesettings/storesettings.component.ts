import { Component, ElementRef, ViewChild, HostListener } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { StoreSettingsService } from '../../services/storesettings.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { MatSidenav } from "@angular/material";

@Component({
    selector: 'store-settings',
    providers: [LoginService, StoreSettingsService],
    templateUrl: 'storesettings.html',
    styleUrls: ['storesettings.css']
})

export class storesettings {


  public storesDB = new PouchDB('steelstores');
  public localStoresDB = new PouchDB('http://localhost:5984/localstores');
  public ibmStoresDB = new PouchDB('https://dewansteel.cloudant.com/dewanstores', {
    auth: {
      'username': 'dewansteel', 
      'password': 'dewansteel@eyetea.co', 
    }
  });

  public cloudantStoresDB = new PouchDB('https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com/dewanstores', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
  });
  
  @ViewChild('sidenav') sidenav: MatSidenav;
  navMode = 'side';
  addNewStoreForm: FormGroup;
  allStores: any = []
  authUser: any = null



  constructor(private _loginService: LoginService,
              private _storeSettingsService: StoreSettingsService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddStoreForm();
    this.authUser = JSON.parse(localStorage.getItem('user'))
  }

  private _buildAddStoreForm(){
    this.addNewStoreForm = this._formBuilder.group({
      name: ['', Validators.required]
    })
  }

  ngOnInit(){
    this.getAllStores()
    this.syncDb()
  }

  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.storesDB.replicate.from(self.cloudantStoresDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY STORES REPLICATION:--", info)
      self.storesDB.sync(self.cloudantStoresDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY CLOUDANT PRODUCTS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.getAllStores()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC CLOUDANT STORES", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY STORES CLOUDANT SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC CLOUDANT STORES!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT STORES!!", err)
    })
  }

  getAllStores(){
    var self = this;
    self._storeSettingsService.allStores().then(function(result){
      console.log("Reuslt after fetching all the company info:---", result)
      self.allStores = []
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      console.log("ALL STORES:(((((((((((", self.allStores)
    }).catch(function(err){
      console.log(err)
    })    
  }

  addStore(values, $event){
    var self = this;
    console.log("VALUES FROM ADD Store:--", values)
    self._storeSettingsService.addStore(values).then(function(result){
      console.log("result after adding Store:---", result)
      self.getAllStores()
      self._buildAddStoreForm()
    }).catch(function(err){
      console.log(err)
    })
  }

  @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth < 886) {
            this.navMode = 'over';
            this.sidenav.close();
        }
        if (event.target.innerWidth > 886) {
           this.navMode = 'side';
           this.sidenav.open();
        }
    }

  logout(){
    this._loginService.logout()
  }  

}