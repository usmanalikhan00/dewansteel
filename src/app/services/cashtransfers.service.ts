import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class CashTransferService {
  
  public cashTransfersDB = new PouchDB('steelcashtransfers');
  // public productCategoriesDB = new PouchDB('productcategories');
  // public productSubCategoriesDB = new PouchDB('productsubcategories');

  constructor(private _router: Router){}

  addCashTransfer(transferObject){
    var self = this;
    return self.cashTransfersDB.put(transferObject);
  }

  allCashTransfers(){
    var self = this;
    return self.cashTransfersDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  // updateCustomerBalance(customerDoc){
  //   var self = this
  //   return self.customersDB.put(customerDoc)
  // }

  // updateCustomers(allCustomers){
  //   var self = this
  //   return self.customersDB.bulkDocs(allCustomers)
  // }
  

  getSingleTrasnfer(transferId){
    var self = this;
    return self.cashTransfersDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.cashTransfersDB.find({
        selector: {_id:{$eq: transferId}}
      });
    })
  }


  

}